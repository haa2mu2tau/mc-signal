#ifndef helper_functions
#define helper_functions

#include <vector>
#include "ROOT/RVec.hxx"

#include "AthContainers/DataVector.h"
#include "AthContainers/ConstDataVector.h"
#include "AthContainers/AuxStoreInternal.h"

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODBase/IParticleHelpers.h"


// Unpack the return value of a method in each element of a container of pointers
template <class T, typename C = DataVector<T>, typename V = float, typename ... As, typename ... As_fn>
ROOT::RVec<V> unpack(const C& container, V (T::*method)(As_fn...) const, const As&... args) {
    ROOT::RVec<V> ret;
    for(const T* elem : container) ret.push_back((elem->*method)(args...));
    return ret;
}

// Unpack an auxiliary variable in each element of a container of pointers
template <typename V, class T = xAOD::IParticle, typename C = DataVector<T>>
ROOT::RVec<V> unpackAux(const C& container, const std::string& property) {
    const SG::AuxElement::ConstAccessor<V> accessor(property);
    ROOT::RVec<V> ret;
    for(const T* elem : container) ret.push_back(accessor(*elem));
    return ret;
}

// Turn a DataVector into an RVec of contant pointers
template <class T>
ROOT::RVec<const T*> buildContainerRVec(const DataVector<T>& container) {
    return ROOT::RVec<const T*>(container.begin(), container.end());
}

// Sort an RVec of const xAOD::IParticle* based on pT
template <class T>
ROOT::RVec<const T*> sortParticleRVec(const ROOT::RVec<const T*>& container) {
    return Sort(container, [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b) { return a->pt() > b->pt(); });
}

// Returns first ancestor
const xAOD::TruthParticle* getFirstAncestor(const xAOD::TruthParticle* p) {
    if(!p->nParents()) return p;
    if(p->parent()->pdgId() != p->pdgId()) return p->parent();
    return getFirstAncestor(p->parent());
}

// Returns the first particle that has different parents, or more than one parent
const xAOD::TruthParticle* getFirst(const xAOD::TruthParticle* p) {
    if(p->nParents() != 1 || p->parent()->pdgId() != p->pdgId()) return p;
    return getFirst(p->parent());
}

// Returns the last particle of the same type
// Careful! This is meaningless in cases of multiple decays of the same type (e.g. g->gg)
const xAOD::TruthParticle* getFinal(const xAOD::TruthParticle* p) {
    for(size_t i = 0; i < p->nChildren(); i++) {
        if(p->child(i)->pdgId() == p->pdgId()) return getFinal(p->child(i));
    }
    return p;
}

// Checks if this is the first particle of the radiation chain
bool isFirst(const xAOD::TruthParticle* p) {
    for(size_t i = 0; i < p->nParents(); i++) {
        if(p->parent(i)->pdgId() != p->pdgId()) return true;
    }
    return false;
}

// Checks if this is the last particle of the radiation chain
bool isFinal(const xAOD::TruthParticle* p) {
    for(size_t i = 0; i < p->nChildren(); i++) {
        if(p->child(i)->pdgId() == p->pdgId()) return false;
    }
    return true;
}

bool isLeptonicETau(const xAOD::TruthParticle* p) {
    for(std::size_t i = 0; i < p->nChildren(); ++i) {
        const xAOD::TruthParticle* child = p->child(i);
        if(child->isElectron()) return true;
    }
    return false;
}

bool isLeptonicMuTau(const xAOD::TruthParticle* p) {
    for(std::size_t i = 0; i < p->nChildren(); ++i) {
        const xAOD::TruthParticle* child = p->child(i);
        if(child->isMuon()) return true;
    }
    return false;
}

// Check if the particle is a Z boson
bool isZ(const xAOD::TruthParticle* p) { return p->isZ(); }

// Check if the particle is a higgs (PDG ID = 35, because we replaced h with H in our JOs for Pythia)
bool isHiggs(const xAOD::TruthParticle* p) { return p->pdgId() == 35; }

// Check if the particle is an pseudo-scalar A (PDG ID = 36)
bool isA(const xAOD::TruthParticle* p) { return p->pdgId() == 36; }

// Classify pseudo-scalar A decay
int classifyA(const xAOD::TruthParticle* p) {
    if(!p || p->pdgId() != 36) return 0;
    int a_mu = 0;
    for(size_t i = 0; i < p->nChildren(); i++) if(p->child(i)->isMuon()) a_mu++;
    if(a_mu == 2) return 1;

    int n_tau_mu = 0, n_tau_e = 0, n_tau_had = 0;
    for(size_t i = 0; i < p->nChildren(); i++) {
        if(isLeptonicMuTau(p->child(i))) n_tau_mu++;
        else if(isLeptonicETau(p->child(i))) n_tau_e++;
        else n_tau_had++;
    }
    if(n_tau_mu == 1 && n_tau_had == 1) return 2;
    else if(n_tau_e == 1 && n_tau_had == 1) return 3;
    else if(n_tau_had == 2) return 4;
    else return 0;
}

int classifyAFromMatchedChildren(const xAOD::TruthParticle* p, const ROOT::RVec<const xAOD::TruthParticle*>& matched_taus, const ROOT::RVec<const xAOD::TruthParticle*>& matched_muons) {
    if(!p || p->pdgId() != 36) return 0;
    if(matched_muons.size() == 2) return 1;

    int n_tau_mu = 0, n_tau_e = 0, n_tau_had = 0;
    for(const xAOD::TruthParticle* t : matched_taus) {
        if(isLeptonicMuTau(t)) n_tau_mu++;
        else if(isLeptonicETau(t)) n_tau_e++;
        else n_tau_had++;
    }
    if(n_tau_mu == 1 && n_tau_had == 1) return 2;
    else if(n_tau_e == 1 && n_tau_had == 1) return 3;
    else if(n_tau_had == 2) return 4;
    else return 5; // Fully leptonic decay
}

const xAOD::TruthParticle* findMatch(const xAOD::TruthParticle* p, const ROOT::RVec<const xAOD::TruthParticle*>& container, const float threshold = 0.02) {
    for(const xAOD::TruthParticle* q : container) {
        if(p->p4().DeltaR(q->p4()) < threshold) return q;
    }
    return nullptr;
}

float dRADecay(const ROOT::RVec<const xAOD::TruthParticle*>& matched_taus, const ROOT::RVec<const xAOD::TruthParticle*>& matched_muons) {
    if(matched_muons.size() == 2) { // 2mu decay
        return matched_muons[0]->p4().DeltaR(matched_muons[1]->p4());
    } else if(matched_taus.size() == 2) { // 2tau decay
        const xAOD::TruthParticle* tau_0 = matched_taus[0];
        const xAOD::TruthParticle* tau_1 = matched_taus[1];
        if(!tau_0 || !tau_1) return -1; // Safeguard against a missing Tau?

        return ROOT::VecOps::DeltaR(
            tau_0->auxdata<double>("eta_vis"),
            tau_1->auxdata<double>("eta_vis"),
            tau_0->auxdata<double>("phi_vis"),
            tau_1->auxdata<double>("phi_vis")
        );
    } else {
        return -1;
    }
}

const TLorentzVector TLVADecay(const ROOT::RVec<const xAOD::TruthParticle*>& matched_taus, const ROOT::RVec<const xAOD::TruthParticle*>& matched_muons) {
    if(matched_muons.size() == 2) { // 2mu decay
        return matched_muons[0]->p4() + matched_muons[1]->p4();
    } else if(matched_taus.size() == 2) { // 2tau decay
        const xAOD::TruthParticle* tau_0 = matched_taus[0];
        const xAOD::TruthParticle* tau_1 = matched_taus[1];
        if(!tau_0 || !tau_1) return TLorentzVector(); // Safeguard against a missing Tau?

        TLorentzVector v_tau_0, v_tau_1;
        v_tau_0.SetPtEtaPhiM(tau_0->auxdata<double>("pt_vis"), tau_0->auxdata<double>("eta_vis"), tau_0->auxdata<double>("phi_vis"), tau_0->auxdata<double>("m_vis"));
        v_tau_1.SetPtEtaPhiM(tau_1->auxdata<double>("pt_vis"), tau_1->auxdata<double>("eta_vis"), tau_1->auxdata<double>("phi_vis"), tau_1->auxdata<double>("m_vis"));

        return v_tau_0 + v_tau_1;
    } else {
        return TLorentzVector();
    }
}

bool isDescendant(const xAOD::TruthParticle* descendant, const xAOD::TruthParticle* ancestor) {
    if(descendant->pt() == ancestor->pt() && descendant->eta() == ancestor->eta() && descendant->phi() == ancestor->phi()) return true;
    if(!descendant->parent()) return false;
    return isDescendant(descendant->parent(), ancestor);
}

bool isDescendantOfPdgId(const xAOD::TruthParticle* descendant, const int ancestor_pdgId) {
    if(!descendant->nParents()) return false;
    for(size_t i = 0; i < descendant->nParents(); i++) {
        if(descendant->parent(i)->pdgId() == ancestor_pdgId) return true;
        if(isDescendantOfPdgId(descendant->parent(i), ancestor_pdgId)) return true;
    }
    return false;
}

const ROOT::RVec<TLorentzVector> createDiMuonVector(const ROOT::RVec<const xAOD::TruthParticle*> muons, const float threshold = 0.6) {
    ROOT::RVec<TLorentzVector> ret;
    for(size_t i = 0; i < muons.size(); i++) {
        const xAOD::TruthParticle* p1 = muons.at(i);
        for(size_t j = 0; j < muons.size(); j++) {
            if(i == j) continue;
            const xAOD::TruthParticle* p2 = muons.at(j);

            const float dR = p1->p4().DeltaR(p2->p4());

            ret.push_back(p1->p4() + p2->p4());
        }
    }
    return ret;
}

const ROOT::RVec<TLorentzVector> createDiTauLepHadVector(const ROOT::RVec<const xAOD::TruthParticle*> taus, const ROOT::RVec<const xAOD::TruthParticle*> others, const float threshold = 0.4, bool remove_direct_overlap = false) {
    ROOT::RVec<TLorentzVector> ret;
    //for(size_t i = 0; i < taus.size(); i++) {
    //    const xAOD::TruthParticle* p1 = taus.at(i);
    //    for(size_t j = 0; j < others.size(); j++) {
    //        const xAOD::TruthParticle* p2 = taus.at(i);
    //        if(remove_direct_overlap && i == j) continue;
    //        if(p1->pt() == ancestor->pt() && descendant->eta() == ancestor->eta() && descendant->phi() == ancestor->phi()) return true;
    //        const xAOD::TruthParticle* p2 = muons.at(j);

    //        const float dR = p1->p4().DeltaR(p2->p4());

    //        ret.push_back(p1->p4() + p2->p4());
    //    }
    //}
    return ret;
}

// Test wrapper
template <class T>
ROOT::RVec<const T*> TestWrapper(ROOT::RVec<const T*> input) {
    std::cout << ">>>> " << input.size() << std::endl;
    return input;
}

#endif
