All the truth analyses are defined here. Each file must contain a function with the following signature:
```
def run_analysis(df: ROOT.RDataFrame, dsid: int, run: int) -> Tuple[ROOT.RDataFrame, List[ROOT.RResultPtr]]
```

The file `helper_functions.h` is dynamically compiled and included in the ROOT environment, so all methods defined there can be used in the truth analyses as `ROOT.<fn-name>(...)`.
