import os
#from lib.utils import ana_home

import ROOT

def run_analysis(df: ROOT.RDataFrame, dsid: int, run: int):
    df = df.Define('TruthParticlesVec', 'buildContainerRVec(TruthParticles)') \
            .Define('TruthElectronsVec', 'buildContainerRVec(TruthElectrons)') \
            .Define('TruthMuonsVec', 'buildContainerRVec(TruthMuons)') \
            .Define('TruthTausVec', 'buildContainerRVec(TruthTaus)') \
            .Define('TruthParticlesFinalVec', 'Map(TruthParticlesVec, &isFinal)')

    hists = []

    # MET
    df = df.Define('met_et', 'MET_Truth.at(0)->met() * 1e-3')

    hists += [
        df.Histo1D(('met_et', ';E_{T}^{miss} [GeV];Events / 3 GeV', 100, 0, 300), 'met_et'),
    ]

    # Single lepton variables
    df = df.Define('mu_pt', 'unpack(TruthMuons, &xAOD::TruthParticle::pt) * 1e-3') \
                .Define('mu_eta', 'unpack(TruthMuons, &xAOD::TruthParticle::eta)') \
                .Define('mu_phi', 'unpack(TruthMuons, &xAOD::TruthParticle::phi)') \
                .Define('mu_n', 'mu_pt.size()') \
                .Define('mu_good', 'mu_pt > 5.0 && abs(mu_eta) < 2.7') \
                .Define('mu_good_n', 'Sum(mu_good)') \
                .Define('el_pt', 'unpack(TruthElectrons, &xAOD::TruthParticle::pt) * 1e-3') \
                .Define('el_eta', 'unpack(TruthElectrons, &xAOD::TruthParticle::eta)') \
                .Define('el_phi', 'unpack(TruthElectrons, &xAOD::TruthParticle::phi)') \
                .Define('el_n', 'el_pt.size()') \
                .Define('el_good', 'el_pt > 4.0 && abs(el_eta) < 2.47 && (abs(el_eta) <= 1.37 || abs(el_eta) >= 1.52)') \
                .Define('el_good_n', 'Sum(el_good)') \
                .Define('tau_pt', 'unpack(TruthTaus, &xAOD::TruthParticle::pt) * 1e-3') \
                .Define('tau_eta', 'unpack(TruthTaus, &xAOD::TruthParticle::eta)') \
                .Define('tau_phi', 'unpack(TruthTaus, &xAOD::TruthParticle::phi)') \
                .Define('tau_pt_vis', 'unpackAux<double>(TruthTaus, "pt_vis") * 1e-3') \
                .Define('tau_eta_vis', 'unpackAux<double>(TruthTaus, "eta_vis")') \
                .Define('tau_phi_vis', 'unpackAux<double>(TruthTaus, "phi_vis")') \
                .Define('tau_eta_vis_diff', 'tau_eta - tau_eta_vis') \
                .Define('tau_phi_vis_diff', 'tau_phi - tau_phi_vis') \
                .Define('tau_met_dphi', 'ROOT::VecOps::DeltaPhi((double)MET_Truth.at(0)->phi(), tau_phi)') \
                .Define('tau_vis_met_dphi', 'ROOT::VecOps::DeltaPhi((double)MET_Truth.at(0)->phi(), tau_phi_vis)') \
                .Define('tau_hadronic', 'unpackAux<char>(TruthTaus, "IsHadronicTau")') \
                .Define('tau_good_hadronic', 'tau_hadronic && tau_pt_vis > 15.0 && abs(tau_eta_vis) < 2.7 && (abs(tau_eta_vis) <= 1.37 || abs(tau_eta_vis) >= 1.52)') \
                .Define('tau_leptonic_e', 'Map(TruthTausVec, &isLeptonicETau)') \
                .Define('tau_good_leptonic_e', 'tau_leptonic_e && tau_pt_vis > 4.0 && abs(tau_eta_vis) < 2.47 && (abs(tau_eta_vis) <= 1.37 || abs(tau_eta_vis) >= 1.52)') \
                .Define('tau_leptonic_mu', 'Map(TruthTausVec, &isLeptonicMuTau)') \
                .Define('tau_good_leptonic_mu', 'tau_leptonic_mu && tau_pt_vis > 5.0 && abs(tau_eta_vis) < 2.7') \
                .Define('tau_leptonic', '!tau_hadronic') \
                .Define('tau_good_leptonic', 'tau_leptonic && (tau_good_leptonic_e || tau_good_leptonic_mu)') \
                .Define('tau_n', 'tau_pt.size()') \
                .Define('tau_final_n', 'Sum(Map(TruthTausVec, &isFinal))') \
                .Define('tau_good', 'tau_good_hadronic || tau_good_leptonic') \
                .Define('tau_good_n', 'Sum(tau_good)') \
                .Define('tau_good_hadronic_n', 'Sum(tau_good_hadronic)') \
                .Define('tau_good_leptonic_mu_n', 'Sum(tau_good_leptonic_mu)') \
                .Define('tau_good_leptonic_e_n', 'Sum(tau_good_leptonic_e)')

    hists += [
        df.Histo1D(('mu_all_pt', ';All Muon p_{T} [GeV];Events / 2 GeV', 100, 0, 200), 'mu_pt'),
        df.Histo1D(('mu_all_eta', ';All Muon #eta;Events / 0.06', 100, -3.0, 3.0), 'mu_eta'),
        df.Histo1D(('mu_all_phi', ';All Muon #phi;Events / 0.064', 100, -3.2, 3.2), 'mu_phi'),
        df.Histo1D(('el_all_pt', ';All Electron p_{T} [GeV];Events / 2 GeV', 100, 0, 200), 'el_pt'),
        df.Histo1D(('el_all_eta', ';All Electron #eta;Events / 0.06', 100, -3.0, 3.0), 'el_eta'),
        df.Histo1D(('el_all_phi', ';All Electron #phi;Events / 0.064', 100, -3.2, 3.2), 'el_phi'),
        df.Histo1D(('tau_all_pt', ';All Tau p_{T} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt'),
        df.Histo1D(('tau_all_eta', ';All Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta'),
        df.Histo1D(('tau_all_phi', ';All Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi'),
        df.Histo1D(('tau_all_pt_vis', ';All Tau p_{T}^{vis} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt_vis'),
        df.Histo1D(('tau_all_eta_vis', ';All Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis'),
        df.Histo1D(('tau_all_phi_vis', ';All Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis'),
        df.Histo1D(('tau_all_eta_vis_diff', ';All Tau #eta - #eta_{vis};Events / 0.06', 100, -0.1, 0.1), 'tau_eta_vis_diff'),
        df.Histo1D(('tau_all_phi_vis_diff', ';All Tau #phi - #phi_{vis};Events / 0.064', 100, -0.1, 0.1), 'tau_phi_vis_diff'),
        df.Histo1D(('tau_all_met_dphi', ';All Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi'),
        df.Histo1D(('tau_all_vis_met_dphi', ';All Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi'),
        df.Histo1D(('mu_all_n', ';N_{#mu} All;Events', 10, 0, 10), 'mu_n'),
        df.Histo1D(('el_all_n', ';N_{e} All;Events', 10, 0, 10), 'el_n'),
        df.Histo1D(('tau_all_n', ';N_{#tau} All;Events', 10, 0, 10), 'tau_n'),
        df.Histo1D(('mu_good_n', ';N_{#mu};Events', 10, 0, 10), 'mu_good_n'),
        df.Histo1D(('el_good_n', ';N_{e};Events', 10, 0, 10), 'el_good_n'),
        df.Histo1D(('tau_good_n', ';N_{#tau};Events', 10, 0, 10), 'tau_good_n'),
    ]

    df = df.Define('TruthMuonsGoodOrderedVec', 'sortParticleRVec(TruthMuonsVec[mu_good && Map(TruthMuonsVec, &isFinal)])') \
            .Define('TruthElectronsGoodOrderedVec', 'sortParticleRVec(TruthElectronsVec[el_good && Map(TruthElectronsVec, &isFinal)])')

    
    # Filtering
    #Preselection:
    df = df.Filter('mu_good_n >= 2', 'N_{#mu} #geq 2') \
            .Filter('tau_good_hadronic_n >= 1', 'N_{#tau}^{had} #geq 1')
            #.Filter('Sum(mu_pt >= 14 && mu_good) >= 2 || (Sum(mu_pt >= 20 && mu_good) >= 1 && Sum(mu_pt >= 8 && mu_good) >= 1)', '2#mu trigger') \

    # Channel selection
    # Of course, this is extremely rough: it ignores the event topology!
    # We wouldn't probably discard events with an electron if it was dR>1 of the tau_had...
    # MuHad channel:
    #df = df.Filter('mu_good_n == 3 && el_good_n == 0 && tau_good_hadronic_n == 1', 'MuHad channel')
    # EHad channel:
    #df = df.Filter('mu_good_n == 2 && el_good_n == 1 && tau_good_hadronic_n == 1', 'EHad channel')
    # HadHad channel
    #df = df.Filter('mu_good_n == 2 && el_good_n == 0 && tau_good_hadronic_n == 2', 'HadHad channel')

    hists += [
        df.Histo1D(('mu_pt', ';Muon p_{T} [GeV];Events / 2 GeV', 100, 0, 200), 'mu_pt', 'mu_good'),
        df.Histo1D(('mu_eta', ';Muon #eta;Events / 0.06', 100, -3.0, 3.0), 'mu_eta', 'mu_good'),
        df.Histo1D(('mu_phi', ';Muon #phi;Events / 0.064', 100, -3.2, 3.2), 'mu_phi', 'mu_good'),

        df.Histo1D(('el_pt', ';Electron p_{T} [GeV];Events / 2 GeV', 100, 0, 200), 'el_pt', 'el_good'),
        df.Histo1D(('el_eta', ';Electron #eta;Events / 0.06', 100, -3.0, 3.0), 'el_eta', 'el_good'),
        df.Histo1D(('el_phi', ';Electron #phi;Events / 0.064', 100, -3.2, 3.2), 'el_phi', 'el_good'),

        df.Histo1D(('tau_pt', ';Tau p_{T} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt', 'tau_good'),
        df.Histo1D(('tau_eta', ';Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta', 'tau_good'),
        df.Histo1D(('tau_phi', ';Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi', 'tau_good'),
        df.Histo1D(('tau_pt_vis', ';Tau p_{T}^{vis} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt_vis', 'tau_good'),
        df.Histo1D(('tau_eta_vis', ';Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis', 'tau_good'),
        df.Histo1D(('tau_phi_vis', ';Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis', 'tau_good'),
        df.Histo1D(('tau_met_dphi', ';Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi', 'tau_good'),
        df.Histo1D(('tau_vis_met_dphi', ';Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi', 'tau_good'),

        df.Histo1D(('tau_had_pt', ';Had Tau p_{T} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_eta', ';Had Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_phi', ';Had Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_pt_vis', ';Had Tau p_{T}^{vis} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt_vis', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_eta_vis', ';Had Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_phi_vis', ';Had Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_met_dphi', ';Had Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_vis_met_dphi', ';Had Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi', 'tau_good_hadronic'),

        df.Histo1D(('tau_e_pt', ';Lep e Tau p_{T} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_eta', ';Lep e Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_phi', ';Lep e Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_pt_vis', ';Lep e Tau p_{T}^{vis} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt_vis', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_eta_vis', ';Lep e Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_phi_vis', ';Lep e Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_met_dphi', ';Lep e Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_vis_met_dphi', ';Lep e Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi', 'tau_good_leptonic_e'),

        df.Histo1D(('tau_mu_pt', ';Lep #mu Tau p_{T} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_eta', ';Lep #mu Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_phi', ';Lep #mu Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_pt_vis', ';Lep #mu Tau p_{T}^{vis} [GeV];Events / 3 GeV', 100, 0, 300), 'tau_pt_vis', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_eta_vis', ';Lep #mu Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_phi_vis', ';Lep #mu Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_met_dphi', ';Lep #mu Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_vis_met_dphi', ';Lep #mu Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi', 'tau_good_leptonic_mu'),
    ]


    # Higgs variables
    df = df.Define('HiggsFinal', 'TruthParticlesVec[Map(TruthParticlesVec, &isHiggs) && TruthParticlesFinalVec][0]') \
            .Define('higgs_pt', 'HiggsFinal->pt() * 1e-3') \
            .Define('higgs_eta', 'HiggsFinal->eta()') \
            .Define('higgs_phi', 'HiggsFinal->phi()')

    hists += [
        df.Histo1D(('higgs_pt', ';Higgs p_{T} [GeV];Events/3 GeV', 100, 0, 300), 'higgs_pt'),
        df.Histo1D(('higgs_eta', ';Higgs #eta;Events/0.06', 100, -3.0, 3.0), 'higgs_eta'),
        df.Histo1D(('higgs_phi', ';Higgs #phi;Events/0.064', 100, -3.2, 3.2), 'higgs_phi'),
    ]

    # a variables
    df = df.Define('AFinalVec', 'return Sort(TruthParticlesVec[Map(TruthParticlesVec, &isA) && TruthParticlesFinalVec], [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b) { return a->pt() > b->pt(); });') \
            .Define('AFinalLead', 'AFinalVec[0]') \
            .Define('AFinalSubl', 'AFinalVec[1]') \
            .Define('a_dr', 'AFinalLead->p4().DeltaR(AFinalSubl->p4())') \
            .Define('a_dphi', 'AFinalLead->p4().DeltaPhi(AFinalSubl->p4())') \
            .Define('a_deta', 'AFinalLead->eta() - AFinalSubl->eta()') \
            .Define('a_lead_pt', 'AFinalLead->pt() * 1e-3') \
            .Define('a_lead_eta', 'AFinalLead->eta()') \
            .Define('a_lead_phi', 'AFinalLead->phi()') \
            .Define('a_lead_m', 'AFinalLead->m() * 1e-3') \
            .Define('AFinalLead_decay_TausVec', 'return Sort(TruthTausVec[Map(TruthTausVec, [&AFinalLead](const xAOD::TruthParticle* p){ return isDescendant(p, AFinalLead); })], [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b) { return a->pt() > b->pt(); });') \
            .Define('AFinalLead_decay_MuonsVec', 'return Sort(TruthMuonsVec[Map(TruthMuonsVec, [&AFinalLead](const xAOD::TruthParticle* p){ return isDescendant(p, AFinalLead) && !isDescendantOfPdgId(p, 15) && !isDescendantOfPdgId(p, -15); })], [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b) { return a->pt() > b->pt(); });') \
            .Define('a_lead_decay', 'classifyAFromMatchedChildren(AFinalLead, AFinalLead_decay_TausVec, AFinalLead_decay_MuonsVec)') \
            .Define('a_lead_decay_taus_dr', 'dRADecay(AFinalLead_decay_TausVec, {})') \
            .Define('AFinalLead_decay_TausTLV', 'TLVADecay(AFinalLead_decay_TausVec, {})') \
            .Define('a_lead_decay_taus_pt', 'AFinalLead_decay_TausTLV.Pt() ? AFinalLead_decay_TausTLV.Pt() * 1e-3 : -10') \
            .Define('a_lead_decay_taus_eta', 'AFinalLead_decay_TausTLV.Pt() ? AFinalLead_decay_TausTLV.Eta() : -10') \
            .Define('a_lead_decay_taus_phi', 'AFinalLead_decay_TausTLV.Pt() ? AFinalLead_decay_TausTLV.Phi() : -10') \
            .Define('a_lead_decay_taus_m', 'AFinalLead_decay_TausTLV.Pt() ? AFinalLead_decay_TausTLV.M() * 1e-3 : -10') \
            .Define('a_lead_decay_taus_lead_pt', 'AFinalLead_decay_TausVec.size() >= 1 ? AFinalLead_decay_TausVec[0]->pt() * 1e-3 : -1') \
            .Define('a_lead_decay_taus_subl_pt', 'AFinalLead_decay_TausVec.size() >= 2 ? AFinalLead_decay_TausVec[1]->pt() * 1e-3 : -1') \
            .Define('a_lead_decay_mus_dr', 'dRADecay({}, AFinalLead_decay_MuonsVec)') \
            .Define('AFinalLead_decay_MuonsTLV', 'TLVADecay({}, AFinalLead_decay_MuonsVec)') \
            .Define('a_lead_decay_mus_pt', 'AFinalLead_decay_MuonsTLV.Pt() ? AFinalLead_decay_MuonsTLV.Pt() * 1e-3 : -10') \
            .Define('a_lead_decay_mus_eta', 'AFinalLead_decay_MuonsTLV.Pt() ? AFinalLead_decay_MuonsTLV.Eta() : -10') \
            .Define('a_lead_decay_mus_phi', 'AFinalLead_decay_MuonsTLV.Pt() ? AFinalLead_decay_MuonsTLV.Phi() : -10') \
            .Define('a_lead_decay_mus_m', 'AFinalLead_decay_MuonsTLV.Pt() ? AFinalLead_decay_MuonsTLV.M() * 1e-3 : -10') \
            .Define('a_lead_decay_mus_lead_pt', 'AFinalLead_decay_MuonsVec.size() >= 1 ? AFinalLead_decay_MuonsVec[0]->pt() * 1e-3 : -1') \
            .Define('a_lead_decay_mus_subl_pt', 'AFinalLead_decay_MuonsVec.size() >= 2 ? AFinalLead_decay_MuonsVec[1]->pt() * 1e-3 : -1') \
            .Define('a_lead_decay_dr', 'dRADecay(AFinalLead_decay_TausVec, AFinalLead_decay_MuonsVec)') \
            .Define('a_subl_pt', 'AFinalSubl->pt() * 1e-3') \
            .Define('a_subl_eta', 'AFinalSubl->eta()') \
            .Define('a_subl_phi', 'AFinalSubl->phi()') \
            .Define('a_subl_m', 'AFinalLead->m() * 1e-3') \
            .Define('AFinalSubl_decay_TausVec', 'return Sort(TruthTausVec[Map(TruthTausVec, [&AFinalSubl](const xAOD::TruthParticle* p){ return isDescendant(p, AFinalSubl); })], [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b) { return a->pt() > b->pt(); });') \
            .Define('AFinalSubl_decay_MuonsVec', 'return Sort(TruthMuonsVec[Map(TruthMuonsVec, [&AFinalSubl](const xAOD::TruthParticle* p){ return isDescendant(p, AFinalSubl) && !isDescendantOfPdgId(p, 15) && !isDescendantOfPdgId(p, -15); })], [](const xAOD::TruthParticle* a, const xAOD::TruthParticle* b) { return a->pt() > b->pt(); });') \
            .Define('a_subl_decay', 'classifyAFromMatchedChildren(AFinalSubl, AFinalSubl_decay_TausVec, AFinalSubl_decay_MuonsVec)') \
            .Define('a_subl_decay_dr', 'dRADecay(AFinalSubl_decay_TausVec, AFinalSubl_decay_MuonsVec)') \
            .Define('a_subl_decay_taus_dr', 'dRADecay(AFinalSubl_decay_TausVec, {})') \
            .Define('AFinalSubl_decay_TausTLV', 'TLVADecay(AFinalSubl_decay_TausVec, {})') \
            .Define('a_subl_decay_taus_pt', 'AFinalSubl_decay_TausTLV.Pt() ? AFinalSubl_decay_TausTLV.Pt() * 1e-3: -10') \
            .Define('a_subl_decay_taus_eta', 'AFinalSubl_decay_TausTLV.Pt() ? AFinalSubl_decay_TausTLV.Eta() : -10') \
            .Define('a_subl_decay_taus_phi', 'AFinalSubl_decay_TausTLV.Pt() ? AFinalSubl_decay_TausTLV.Phi() : -10') \
            .Define('a_subl_decay_taus_m', 'AFinalSubl_decay_TausTLV.Pt() ? AFinalSubl_decay_TausTLV.M() * 1e-3 : -10') \
            .Define('a_subl_decay_taus_lead_pt', 'AFinalSubl_decay_TausVec.size() >= 1 ? AFinalSubl_decay_TausVec[0]->pt() * 1e-3 : -1') \
            .Define('a_subl_decay_taus_subl_pt', 'AFinalSubl_decay_TausVec.size() >= 2 ? AFinalSubl_decay_TausVec[1]->pt() * 1e-3 : -1') \
            .Define('a_subl_decay_mus_dr', 'dRADecay({}, AFinalSubl_decay_MuonsVec)') \
            .Define('AFinalSubl_decay_MuonsTLV', 'TLVADecay({}, AFinalSubl_decay_MuonsVec)') \
            .Define('a_subl_decay_mus_pt', 'AFinalSubl_decay_MuonsTLV.Pt() ? AFinalSubl_decay_MuonsTLV.Pt() * 1e-3 : -10') \
            .Define('a_subl_decay_mus_eta', 'AFinalSubl_decay_MuonsTLV.Pt() ? AFinalSubl_decay_MuonsTLV.Eta() : -10') \
            .Define('a_subl_decay_mus_phi', 'AFinalSubl_decay_MuonsTLV.Pt() ? AFinalSubl_decay_MuonsTLV.Phi() : -10') \
            .Define('a_subl_decay_mus_m', 'AFinalSubl_decay_MuonsTLV.Pt() ? AFinalSubl_decay_MuonsTLV.M() * 1e-3 : -10') \
            .Define('a_subl_decay_mus_lead_pt', 'AFinalSubl_decay_MuonsVec.size() >= 1 ? AFinalSubl_decay_MuonsVec[0]->pt() * 1e-3 : -1') \
            .Define('a_subl_decay_mus_subl_pt', 'AFinalSubl_decay_MuonsVec.size() >= 2 ? AFinalSubl_decay_MuonsVec[1]->pt() * 1e-3 : -1')

    hists += [
        df.Histo1D(('a_dr', ';#DeltaR(a, a);Events', 100, 0, 5), 'a_dr'),
        df.Histo1D(('a_dphi', ';#Delta#phi(a, a);Events', 100, -3.2, 3.2), 'a_dphi'),
        df.Histo1D(('a_deta', ';#Delta#eta(a, a) = #eta_{a}^{lead} - #eta_{a}^{subl};Events', 100, -5, 5), 'a_deta'),
        df.Histo1D(('a_lead_pt', ';Lead a p_{T} [GeV];Events/3 GeV', 100, 0, 300), 'a_lead_pt'),
        df.Histo1D(('a_lead_eta', ';Lead a #eta;Events/0.06', 100, -3.0, 3.0), 'a_lead_eta'),
        df.Histo1D(('a_lead_phi', ';Lead a #phi;Events/0.064', 100, -3.2, 3.2), 'a_lead_phi'),
        df.Histo1D(('a_lead_m', ';Lead m_{a} [GeV];Events/0.3 GeV', 100, 0, 30), 'a_lead_m'),
        df.Histo1D(('a_lead_decay', ';Lead a decay;Events', 6, 0, 6), 'a_lead_decay'),
        df.Histo1D(('a_lead_decay_dr', ';Lead a decay #DeltaR;Events', 500, 0, 1.5), 'a_lead_decay_dr'),
        df.Histo1D(('a_lead_decay_taus_dr', ';Lead a#rightarrow#tau#tau decay #DeltaR;Events', 500, 0, 1.5), 'a_lead_decay_taus_dr'),
        df.Histo1D(('a_lead_decay_taus_pt', ';Lead a#rightarrow#tau#tau p_{T} [GeV];Events/3 GeV', 100, 0, 300), 'a_lead_decay_taus_pt'),
        df.Histo1D(('a_lead_decay_taus_eta', ';Lead a#rightarrow#tau#tau #eta;Events/0.06', 100, -3.0, 3.0), 'a_lead_decay_taus_eta'),
        df.Histo1D(('a_lead_decay_taus_phi', ';Lead a#rightarrow#tau#tau #phi;Events/0.064', 100, -3.2, 3.2), 'a_lead_decay_taus_phi'),
        df.Histo1D(('a_lead_decay_taus_m', ';Lead a#rightarrow#tau#tau m_{a} [GeV];Events/0.5 GeV', 100, 0, 30), 'a_lead_decay_taus_m'),
        df.Histo1D(('a_lead_decay_taus_lead_pt', ';Lead a#rightarrow#tau#tau p_{T}^{lead} [GeV];Events/3 GeV', 100, 0, 300), 'a_lead_decay_taus_lead_pt'),
        df.Histo1D(('a_lead_decay_taus_subl_pt', ';Lead a#rightarrow#tau#tau p_{T}^{subl} [GeV];Events/3 GeV', 100, 0, 300), 'a_lead_decay_taus_subl_pt'),
        df.Histo1D(('a_lead_decay_mus_dr', ';Lead a#rightarrow#mu#mu decay #DeltaR;Events', 500, 0, 1.5), 'a_lead_decay_mus_dr'),
        df.Histo1D(('a_lead_decay_mus_pt', ';Lead a#rightarrow#mu#mu p_{T} [GeV];Events/3 GeV', 100, 0, 300), 'a_lead_decay_mus_pt'),
        df.Histo1D(('a_lead_decay_mus_eta', ';Lead a#rightarrow#mu#mu #eta;Events/0.06', 100, -3.0, 3.0), 'a_lead_decay_mus_eta'),
        df.Histo1D(('a_lead_decay_mus_phi', ';Lead a#rightarrow#mu#mu #phi;Events/0.064', 100, -3.2, 3.2), 'a_lead_decay_mus_phi'),
        df.Histo1D(('a_lead_decay_mus_m', ';Lead a#rightarrow#mu#mu m_{a} [GeV];Events/0.5 GeV', 100, 0, 30), 'a_lead_decay_mus_m'),
        df.Histo1D(('a_lead_decay_mus_lead_pt', ';Lead a#rightarrow#mu#mu p_{T}^{lead} [GeV];Events/3 GeV', 100, 0, 300), 'a_lead_decay_mus_lead_pt'),
        df.Histo1D(('a_lead_decay_mus_subl_pt', ';Lead a#rightarrow#mu#mu p_{T}^{subl} [GeV];Events/3 GeV', 100, 0, 300), 'a_lead_decay_mus_subl_pt'),
        df.Histo1D(('a_subl_pt', ';Sublead a p_{T} [GeV];Events/3 GeV', 100, 0, 300), 'a_subl_pt'),
        df.Histo1D(('a_subl_eta', ';Sublead a #eta;Events/0.06', 100, -3.0, 3.0), 'a_subl_eta'),
        df.Histo1D(('a_subl_phi', ';Sublead a #phi;Events/0.064', 100, -3.2, 3.2), 'a_subl_phi'),
        df.Histo1D(('a_subl_m', ';Sublead m_{a} [GeV];Events/0.3 GeV', 100, 0, 30), 'a_subl_m'),
        df.Histo1D(('a_subl_decay', ';Sublead a decay;Events', 6, 0, 6), 'a_subl_decay'),
        df.Histo1D(('a_subl_decay_dr', ';Sublead a decay #DeltaR;Events', 500, 0, 1.5), 'a_subl_decay_dr'),
        df.Histo1D(('a_subl_decay_taus_dr', ';Sublead a#rightarrow#tau#tau decay #DeltaR;Events', 500, 0, 1.5), 'a_subl_decay_taus_dr'),
        df.Histo1D(('a_subl_decay_taus_pt', ';Sublead a#rightarrow#tau#tau p_{T} [GeV];Events/3 GeV', 100, 0, 300), 'a_subl_decay_taus_pt'),
        df.Histo1D(('a_subl_decay_taus_eta', ';Sublead a#rightarrow#tau#tau #eta;Events/0.06', 100, -3.0, 3.0), 'a_subl_decay_taus_eta'),
        df.Histo1D(('a_subl_decay_taus_phi', ';Sublead a#rightarrow#tau#tau #phi;Events/0.064', 100, -3.2, 3.2), 'a_subl_decay_taus_phi'),
        df.Histo1D(('a_subl_decay_taus_m', ';Sublead a#rightarrow#tau#tau m_{a} [GeV];Events/0.5 GeV', 100, 0, 30), 'a_subl_decay_taus_m'),
        df.Histo1D(('a_subl_decay_taus_lead_pt', ';Sublead a#rightarrow#tau#tau p_{T}^{lead} [GeV];Events/3 GeV', 100, 0, 300), 'a_subl_decay_taus_lead_pt'),
        df.Histo1D(('a_subl_decay_taus_subl_pt', ';Sublead a#rightarrow#tau#tau p_{T}^{subl} [GeV];Events/3 GeV', 100, 0, 300), 'a_subl_decay_taus_subl_pt'),
        df.Histo1D(('a_subl_decay_mus_dr', ';Sublead a#rightarrow#mu#mu decay #DeltaR;Events', 500, 0, 1.5), 'a_subl_decay_mus_dr'),
        df.Histo1D(('a_subl_decay_mus_pt', ';Sublead a#rightarrow#mu#mu p_{T} [GeV];Events/3 GeV', 100, 0, 300), 'a_subl_decay_mus_pt'),
        df.Histo1D(('a_subl_decay_mus_eta', ';Sublead a#rightarrow#mu#mu #eta;Events/0.06', 100, -3.0, 3.0), 'a_subl_decay_mus_eta'),
        df.Histo1D(('a_subl_decay_mus_phi', ';Sublead a#rightarrow#mu#mu #phi;Events/0.064', 100, -3.2, 3.2), 'a_subl_decay_mus_phi'),
        df.Histo1D(('a_subl_decay_mus_m', ';Sublead a#rightarrow#mu#mu m_{a} [GeV];Events/0.5 GeV', 100, 0, 30), 'a_subl_decay_mus_m'),
        df.Histo1D(('a_subl_decay_mus_lead_pt', ';Sublead a#rightarrow#mu#mu p_{T}^{lead} [GeV];Events/3 GeV', 100, 0, 300), 'a_subl_decay_mus_lead_pt'),
        df.Histo1D(('a_subl_decay_mus_subl_pt', ';Sublead a#rightarrow#mu#mu p_{T}^{subl} [GeV];Events/3 GeV', 100, 0, 300), 'a_subl_decay_mus_subl_pt'),
    ]

    # a variables
    df = df.Define('AFinalMuMu', 'a_lead_decay == 1 ? AFinalLead : AFinalSubl') \
            .Define('AFinalMuMu_decay_MuonsVec', 'a_lead_decay == 1 ? AFinalLead_decay_MuonsVec : AFinalSubl_decay_MuonsVec') \
            .Define('AFinalMuMu_decay_MuonsTLV', 'a_lead_decay == 1 ? AFinalLead_decay_MuonsTLV : AFinalSubl_decay_MuonsTLV') \
            .Define('a_2mu_dr', 'dRADecay({}, AFinalMuMu_decay_MuonsVec)') \
            .Define('a_2mu_pt', 'AFinalMuMu_decay_MuonsTLV.Pt() * 1e-3') \
            .Define('a_2mu_eta', 'AFinalMuMu_decay_MuonsTLV.Eta()') \
            .Define('a_2mu_phi', 'AFinalMuMu_decay_MuonsTLV.Phi()') \
            .Define('a_2mu_m', 'AFinalMuMu_decay_MuonsTLV.M() * 1e-3') \
            .Define('a_2mu_mus_pt', 'AFinalMuMu_decay_MuonsVec[0]->pt() * 1e-3') \
            .Define('a_2mu_mus_lead_pt', 'AFinalMuMu_decay_MuonsVec[0]->pt() * 1e-3') \
            .Define('a_2mu_mus_subl_pt', 'AFinalMuMu_decay_MuonsVec[1]->pt() * 1e-3') \
            .Define('a_2mu_lead_index', '(int)std::distance(TruthMuonsGoodOrderedVec.begin(), std::find(TruthMuonsGoodOrderedVec.begin(), TruthMuonsGoodOrderedVec.end(), AFinalMuMu_decay_MuonsVec[0]))')

    df = df.Define('AFinalTauTau', 'a_lead_decay == 1 ? AFinalSubl : AFinalLead') \
            .Define('AFinalTauTau_decay_TausVec', 'a_lead_decay == 1 ? AFinalSubl_decay_TausVec : AFinalLead_decay_TausVec') \
            .Define('AFinalTauTau_decay_TausTLV', 'a_lead_decay == 1 ? AFinalSubl_decay_TausTLV : AFinalLead_decay_TausTLV') \
            .Define('a_2tau_decay', 'a_lead_decay == 1 ? a_subl_decay - 2 : a_lead_decay - 2') \
            .Define('a_2tau_dr', 'dRADecay(AFinalTauTau_decay_TausVec, {})') \
            .Define('a_2tau_pt', 'AFinalTauTau_decay_TausTLV.Pt() * 1e-3') \
            .Define('a_2tau_eta', 'AFinalTauTau_decay_TausTLV.Eta()') \
            .Define('a_2tau_phi', 'AFinalTauTau_decay_TausTLV.Phi()') \
            .Define('a_2tau_m', 'AFinalTauTau_decay_TausTLV.M() * 1e-3') \
            .Define('a_2tau_taus_pt', 'AFinalTauTau_decay_TausVec[0]->pt() * 1e-3') \
            .Define('a_2tau_taus_lead_pt', 'AFinalTauTau_decay_TausVec[0]->pt() * 1e-3') \
            .Define('a_2tau_taus_subl_pt', 'AFinalTauTau_decay_TausVec[1]->pt() * 1e-3')


    hists += [
        df.Histo1D(('a_2mu_dr', ';a#rightarrow#mu#mu #DeltaR(#mu, #mu);Events', 500, 0, 1.5), 'a_2mu_dr'),
        df.Histo1D(('a_2mu_pt', ';a#rightarrow#mu#mu p_{T}^{#mu#mu} [GeV];Events/3 GeV', 100, 0, 300), 'a_2mu_pt'),
        df.Histo1D(('a_2mu_eta', ';a#rightarrow#mu#mu #eta_{#mu#mu};Events/0.06', 100, -3.0, 3.0), 'a_2mu_eta'),
        df.Histo1D(('a_2mu_phi', ';a#rightarrow#mu#mu #phi_{#mu#mu};Events/0.064', 100, -3.2, 3.2), 'a_2mu_phi'),
        df.Histo1D(('a_2mu_m', ';a#rightarrow#mu#mu m_{#mu#mu} [GeV];Events/0.25 GeV', 100, 0, 25), 'a_2mu_m'),
        df.Histo1D(('a_2mu_mus_pt', ';a#rightarrow#mu#mu p_{T}^{#mu} [GeV];Events/3 GeV', 100, 0, 300), 'a_2mu_mus_pt'),
        df.Histo1D(('a_2mu_mus_lead_pt', ';a#rightarrow#mu#mu p_{T}^{Lead #mu} [GeV];Events/3 GeV', 100, 0, 300), 'a_2mu_mus_lead_pt'),
        df.Histo1D(('a_2mu_mus_subl_pt', ';a#rightarrow#mu#mu p_{T}^{Subl #mu} [GeV];Events/3 GeV', 100, 0, 300), 'a_2mu_mus_subl_pt'),
        df.Histo1D(('a_2mu_lead_index', ';Lead #mu index;Events', 20, 0, 20), 'a_2mu_lead_index'),

        df.Histo1D(('a_2tau_decay', ';a#rightarrow#tau#tau decay channel;Events', 4, 0, 4), 'a_2tau_decay'),
        df.Histo1D(('a_2tau_dr', ';a#rightarrow#tau#tau #DeltaR(#tau, #tau);Events', 500, 0, 1.5), 'a_2tau_dr'),
        df.Histo1D(('a_2tau_pt', ';a#rightarrow#tau#tau p_{T}^{#tau#tau} [GeV];Events/3 GeV', 100, 0, 300), 'a_2tau_pt'),
        df.Histo1D(('a_2tau_eta', ';a#rightarrow#tau#tau #eta_{#tau#tau};Events/0.06', 100, -3.0, 3.0), 'a_2tau_eta'),
        df.Histo1D(('a_2tau_phi', ';a#rightarrow#tau#tau #phi_{#tau#tau};Events/0.064', 100, -3.2, 3.2), 'a_2tau_phi'),
        df.Histo1D(('a_2tau_m', ';a#rightarrow#tau#tau m_{#tau#tau} [GeV];Events/0.25 GeV', 100, 0, 25), 'a_2tau_m'),
        df.Histo1D(('a_2tau_taus_pt', ';a#rightarrow#tau#tau p_{T}^{#tau} [GeV];Events/3 GeV', 100, 0, 300), 'a_2tau_taus_pt'),
        df.Histo1D(('a_2tau_taus_lead_pt', ';a#rightarrow#tau#tau p_{T}^{Lead #tau} [GeV];Events/3 GeV', 100, 0, 300), 'a_2tau_taus_lead_pt'),
        df.Histo1D(('a_2tau_taus_subl_pt', ';a#rightarrow#tau#tau p_{T}^{Subl #tau} [GeV];Events/3 GeV', 100, 0, 300), 'a_2tau_taus_subl_pt'),
    ]

    # In order to efficiently select 
    # Di-muon variables
    #df = df.Define('DiMuTLVVec', '')


    #for n in df.GetColumnNames(): print(f'{n}: {df.GetColumnType(n)}')

    
    return df, hists
