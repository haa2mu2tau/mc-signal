import os
#from lib.utils import ana_home

import ROOT

def run_analysis(df: ROOT.RDataFrame, dsid: int, run: int):
    df = df.Define('TruthElectronsVec', 'buildContainerRVec(TruthElectrons)') \
            .Define('TruthMuonsVec', 'buildContainerRVec(TruthMuons)') \
            .Define('TruthTausVec', 'buildContainerRVec(TruthTaus)') \
            .Define('TruthBSMVec', 'buildContainerRVec(TruthBSM)') \
            .Define('TruthBSMFinalVec', 'TruthBSMVec[Map(TruthBSMVec, &isFinal)]') \
            .Define('TruthBosonVec', 'buildContainerRVec(TruthBoson)') \
            .Define('TruthBosonFinalVec', 'TruthBosonVec[Map(TruthBosonVec, &isFinal)]') \
            .Define('TruthBosonsWithDecayParticlesVec', 'buildContainerRVec(TruthBosonsWithDecayParticles)') \
            .Define('TruthBosonsWithDecayParticlesFinalVec', 'TruthBosonsWithDecayParticlesVec[Map(TruthBosonsWithDecayParticlesVec, &isFinal)]')
            #.Define('TruthBosonVec', 'buildContainerRVec(TruthBosonsWithDecayParticles)')
            #.Define('TruthBosonsWithDecayParticlesVec', 'buildContainerRVec(TruthBosonsWithDecayParticles)')

    hists = []

    # MET
    df = df.Define('met_et', 'MET_Truth.at(0)->met() * 1e-3')

    hists += [
        df.Histo1D(('met_et', ';E_{T}^{miss} [GeV];Events / 0.1 GeV', 100, 0, 10), 'met_et'),
    ]

    # Single lepton variables
    df = df.Define('mu_pt', 'unpack(TruthMuons, &xAOD::TruthParticle::pt) * 1e-3') \
                .Define('mu_eta', 'unpack(TruthMuons, &xAOD::TruthParticle::eta)') \
                .Define('mu_phi', 'unpack(TruthMuons, &xAOD::TruthParticle::phi)') \
                .Define('mu_n', 'mu_pt.size()') \
                .Define('mu_good', 'mu_pt > 5.0 && abs(mu_eta) < 2.7') \
                .Define('mu_good_n', 'Sum(mu_good)') \
                .Define('el_pt', 'unpack(TruthElectrons, &xAOD::TruthParticle::pt) * 1e-3') \
                .Define('el_eta', 'unpack(TruthElectrons, &xAOD::TruthParticle::eta)') \
                .Define('el_phi', 'unpack(TruthElectrons, &xAOD::TruthParticle::phi)') \
                .Define('el_n', 'el_pt.size()') \
                .Define('el_good', 'el_pt > 4.0 && abs(el_eta) < 2.47 && (abs(el_eta) <= 1.37 || abs(el_eta) >= 1.52)') \
                .Define('el_good_n', 'Sum(el_good)') \
                .Define('tau_pt', 'unpack(TruthTaus, &xAOD::TruthParticle::pt) * 1e-3') \
                .Define('tau_eta', 'unpack(TruthTaus, &xAOD::TruthParticle::eta)') \
                .Define('tau_phi', 'unpack(TruthTaus, &xAOD::TruthParticle::phi)') \
                .Define('tau_pt_vis', 'unpackAux<double>(TruthTaus, "pt_vis") * 1e-3') \
                .Define('tau_eta_vis', 'unpackAux<double>(TruthTaus, "eta_vis")') \
                .Define('tau_phi_vis', 'unpackAux<double>(TruthTaus, "phi_vis")') \
                .Define('tau_eta_vis_diff', 'tau_eta - tau_eta_vis') \
                .Define('tau_phi_vis_diff', 'tau_phi - tau_phi_vis') \
                .Define('tau_met_dphi', 'ROOT::VecOps::DeltaPhi((double)MET_Truth.at(0)->phi(), tau_phi)') \
                .Define('tau_vis_met_dphi', 'ROOT::VecOps::DeltaPhi((double)MET_Truth.at(0)->phi(), tau_phi_vis)') \
                .Define('tau_hadronic', 'unpackAux<char>(TruthTaus, "IsHadronicTau")') \
                .Define('tau_good_hadronic', 'tau_hadronic && tau_pt_vis > 15.0 && abs(tau_eta_vis) < 2.7 && (abs(tau_eta_vis) <= 1.37 || abs(tau_eta_vis) >= 1.52)') \
                .Define('tau_leptonic_e', 'Map(TruthTausVec, &isLeptonicETau)') \
                .Define('tau_good_leptonic_e', 'tau_leptonic_e && tau_pt_vis > 4.0 && abs(tau_eta_vis) < 2.47 && (abs(tau_eta_vis) <= 1.37 || abs(tau_eta_vis) >= 1.52)') \
                .Define('tau_leptonic_mu', 'Map(TruthTausVec, &isLeptonicMuTau)') \
                .Define('tau_good_leptonic_mu', 'tau_leptonic_mu && tau_pt_vis > 5.0 && abs(tau_eta_vis) < 2.7') \
                .Define('tau_leptonic', '!tau_hadronic') \
                .Define('tau_good_leptonic', 'tau_leptonic && (tau_good_leptonic_e || tau_good_leptonic_mu)') \
                .Define('tau_n', 'tau_pt.size()') \
                .Define('tau_final_n', 'Sum(Map(TruthTausVec, &isFinal))') \
                .Define('tau_good', 'tau_good_hadronic || tau_good_leptonic') \
                .Define('tau_good_n', 'Sum(tau_good)') \
                .Define('tau_good_hadronic_n', 'Sum(tau_good_hadronic)') \
                .Define('tau_good_leptonic_mu_n', 'Sum(tau_good_leptonic_mu)') \
                .Define('tau_good_leptonic_e_n', 'Sum(tau_good_leptonic_e)')

    hists += [
        df.Histo1D(('mu_all_pt', ';All Muon p_{T} [GeV];Events / 0.3 GeV', 100, 0, 30), 'mu_pt'),
        df.Histo1D(('mu_all_eta', ';All Muon #eta;Events / 0.06', 100, -3.0, 3.0), 'mu_eta'),
        df.Histo1D(('mu_all_phi', ';All Muon #phi;Events / 0.064', 100, -3.2, 3.2), 'mu_phi'),
        df.Histo1D(('el_all_pt', ';All Electron p_{T} [GeV];Events / 0.2 GeV', 100, 0, 20), 'el_pt'),
        df.Histo1D(('el_all_eta', ';All Electron #eta;Events / 0.06', 100, -3.0, 3.0), 'el_eta'),
        df.Histo1D(('el_all_phi', ';All Electron #phi;Events / 0.064', 100, -3.2, 3.2), 'el_phi'),
        df.Histo1D(('tau_all_pt', ';All Tau p_{T} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt'),
        df.Histo1D(('tau_all_eta', ';All Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta'),
        df.Histo1D(('tau_all_phi', ';All Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi'),
        df.Histo1D(('tau_all_pt_vis', ';All Tau p_{T}^{vis} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt_vis'),
        df.Histo1D(('tau_all_eta_vis', ';All Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis'),
        df.Histo1D(('tau_all_phi_vis', ';All Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis'),
        df.Histo1D(('tau_all_eta_vis_diff', ';All Tau #eta - #eta_{vis};Events / 0.06', 100, -0.1, 0.1), 'tau_eta_vis_diff'),
        df.Histo1D(('tau_all_phi_vis_diff', ';All Tau #phi - #phi_{vis};Events / 0.064', 100, -0.1, 0.1), 'tau_phi_vis_diff'),
        df.Histo1D(('tau_all_met_dphi', ';All Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi'),
        df.Histo1D(('tau_all_vis_met_dphi', ';All Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi'),
        df.Histo1D(('mu_all_n', ';N_{#mu} All;Events', 10, 0, 10), 'mu_n'),
        df.Histo1D(('el_all_n', ';N_{e} All;Events', 10, 0, 10), 'el_n'),
        df.Histo1D(('tau_all_n', ';N_{#tau} All;Events', 10, 0, 10), 'tau_n'),
        df.Histo1D(('mu_good_n', ';N_{#mu};Events', 10, 0, 10), 'mu_good_n'),
        df.Histo1D(('el_good_n', ';N_{e};Events', 10, 0, 10), 'el_good_n'),
        df.Histo1D(('tau_good_n', ';N_{#tau};Events', 10, 0, 10), 'tau_good_n'),
    ]

    df = df.Define('TruthMuonsGoodOrderedVec', 'sortParticleRVec(TruthMuonsVec[mu_good && Map(TruthMuonsVec, &isFinal)])') \
            .Define('TruthElectronsGoodOrderedVec', 'sortParticleRVec(TruthElectronsVec[el_good && Map(TruthElectronsVec, &isFinal)])')

    
    # Filtering
    #Preselection:
    #df = df.Filter('mu_good_n >= 2', 'N_{#mu} #geq 2') \
            #.Filter('tau_good_hadronic_n >= 1', 'N_{#tau}^{had} #geq 1')
            #.Filter('Sum(mu_pt >= 14 && mu_good) >= 2 || (Sum(mu_pt >= 20 && mu_good) >= 1 && Sum(mu_pt >= 8 && mu_good) >= 1)', '2#mu trigger') \

    # Channel selection
    # Of course, this is extremely rough: it ignores the event topology!
    # We wouldn't probably discard events with an electron if it was dR>1 of the tau_had...
    # MuHad channel:
    #df = df.Filter('mu_good_n == 3 && el_good_n == 0 && tau_good_hadronic_n == 1', 'MuHad channel')
    # EHad channel:
    #df = df.Filter('mu_good_n == 2 && el_good_n == 1 && tau_good_hadronic_n == 1', 'EHad channel')
    # HadHad channel
    #df = df.Filter('mu_good_n == 2 && el_good_n == 0 && tau_good_hadronic_n == 2', 'HadHad channel')

    hists += [
        df.Histo1D(('mu_pt', ';Muon p_{T} [GeV];Events / 0.3 GeV', 100, 0, 30), 'mu_pt', 'mu_good'),
        df.Histo1D(('mu_eta', ';Muon #eta;Events / 0.06', 100, -3.0, 3.0), 'mu_eta', 'mu_good'),
        df.Histo1D(('mu_phi', ';Muon #phi;Events / 0.064', 100, -3.2, 3.2), 'mu_phi', 'mu_good'),

        df.Histo1D(('el_pt', ';Electron p_{T} [GeV];Events / 0.2 GeV', 100, 0, 20), 'el_pt', 'el_good'),
        df.Histo1D(('el_eta', ';Electron #eta;Events / 0.06', 100, -3.0, 3.0), 'el_eta', 'el_good'),
        df.Histo1D(('el_phi', ';Electron #phi;Events / 0.064', 100, -3.2, 3.2), 'el_phi', 'el_good'),

        df.Histo1D(('tau_pt', ';Tau p_{T} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt', 'tau_good'),
        df.Histo1D(('tau_eta', ';Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta', 'tau_good'),
        df.Histo1D(('tau_phi', ';Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi', 'tau_good'),
        df.Histo1D(('tau_pt_vis', ';Tau p_{T}^{vis} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt_vis', 'tau_good'),
        df.Histo1D(('tau_eta_vis', ';Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis', 'tau_good'),
        df.Histo1D(('tau_phi_vis', ';Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis', 'tau_good'),
        df.Histo1D(('tau_met_dphi', ';Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi', 'tau_good'),
        df.Histo1D(('tau_vis_met_dphi', ';Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi', 'tau_good'),

        df.Histo1D(('tau_had_pt', ';Had Tau p_{T} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_eta', ';Had Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_phi', ';Had Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_pt_vis', ';Had Tau p_{T}^{vis} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt_vis', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_eta_vis', ';Had Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_phi_vis', ';Had Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_met_dphi', ';Had Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi', 'tau_good_hadronic'),
        df.Histo1D(('tau_had_vis_met_dphi', ';Had Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi', 'tau_good_hadronic'),

        df.Histo1D(('tau_e_pt', ';Lep e Tau p_{T} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_eta', ';Lep e Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_phi', ';Lep e Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_pt_vis', ';Lep e Tau p_{T}^{vis} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt_vis', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_eta_vis', ';Lep e Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_phi_vis', ';Lep e Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_met_dphi', ';Lep e Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi', 'tau_good_leptonic_e'),
        df.Histo1D(('tau_e_vis_met_dphi', ';Lep e Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi', 'tau_good_leptonic_e'),

        df.Histo1D(('tau_mu_pt', ';Lep #mu Tau p_{T} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_eta', ';Lep #mu Tau #eta;Events / 0.06', 100, -3.0, 3.0), 'tau_eta', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_phi', ';Lep #mu Tau #phi;Events / 0.064', 100, -3.2, 3.2), 'tau_phi', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_pt_vis', ';Lep #mu Tau p_{T}^{vis} [GeV];Events / 0.6 GeV', 100, 0, 60), 'tau_pt_vis', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_eta_vis', ';Lep #mu Tau #eta_{vis};Events / 0.06', 100, -3.0, 3.0), 'tau_eta_vis', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_phi_vis', ';Lep #mu Tau #phi_{vis};Events / 0.064', 100, -3.2, 3.2), 'tau_phi_vis', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_met_dphi', ';Lep #mu Tau #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_met_dphi', 'tau_good_leptonic_mu'),
        df.Histo1D(('tau_mu_vis_met_dphi', ';Lep #mu Tau_{vis} #Delta#phi(#tau, E_{T}^{miss});Events / 0.064', 100, -3.2, 3.2), 'tau_vis_met_dphi', 'tau_good_leptonic_mu'),
    ]


    # Z boson variables
    df = df.Define('ZVec', 'TruthBosonVec[Map(TruthBosonVec, &isZ)]') \
            .Define('ZFinalVec', 'TruthBosonFinalVec[Map(TruthBosonFinalVec, &isZ)]') \
            .Define('NBoson', 'TruthBoson.size()') \
            .Define('NFinalBoson', 'TruthBosonFinalVec.size()') \
            .Define('NZ', 'ZVec.size()') \
            .Define('NFinalZ', 'ZFinalVec.size()') \
            .Define('NBSM', 'TruthBSMVec.size()') \
            .Define('NFinalBSM', 'TruthBSMFinalVec.size()')
            #.Define('ZFinal', 'ZFinalVec[0]') \
            #.Define('Z_pt', 'ZFinal->pt() * 1e-3') \
            #.Define('Z_eta', 'ZFinal->eta()') \
            #.Define('Z_phi', 'ZFinal->phi()')

    hists += [
        df.Histo1D(('NBosons', ';N Bosons;Events', 10, 0, 10), 'NBoson'),
        df.Histo1D(('NFinalBosons', ';N Final Bosons;Events', 10, 0, 10), 'NFinalBoson'),
        df.Histo1D(('NZ', ';N Z;Events', 10, 0, 10), 'NZ'),
        df.Histo1D(('NFinalZ', ';N Final Z;Events', 10, 0, 10), 'NFinalZ'),
        df.Histo1D(('NBSM', ';N BSM;Events', 10, 0, 10), 'NBSM'),
        df.Histo1D(('NFinalBSM', ';N Final BSM;Events', 10, 0, 10), 'NFinalBSM'),
        #df.Histo1D(('Z_pt', ';Z p_{T} [GeV];Events/3 GeV', 100, 0, 300), 'Z_pt'),
        #df.Histo1D(('Z_eta', ';Z #eta;Events/0.06', 100, -3.0, 3.0), 'Z_eta'),
        #df.Histo1D(('Z_phi', ';Z #phi;Events/0.064', 100, -3.2, 3.2), 'Z_phi'),
    ]

    # 2mu boson variables
    df = df.Define('FinalMuons', 'TruthMuonsVec[Map(TruthMuonsVec, &isFinal)]') \
            .Define('DiMuon', 'FinalMuons.size() >= 2 ? FinalMuons[0]->p4() + FinalMuons[1]->p4() : TLorentzVector()') \
            .Define('DiMuon_pt', 'DiMuon.Pt() * 1e-3') \
            .Define('DiMuon_eta', 'DiMuon.Eta()') \
            .Define('DiMuon_phi', 'DiMuon.Phi()') \
            .Define('DiMuon_m', 'DiMuon.M() * 1e-3')

    hists += [
        df.Histo1D(('DiMuon_pt', ';DiMuon p_{T} [GeV];Events/0.2 GeV', 100, 0, 20), 'DiMuon_pt'),
        df.Histo1D(('DiMuon_eta', ';DiMuon #eta;Events/0.06', 100, -3.0, 3.0), 'DiMuon_eta'),
        df.Histo1D(('DiMuon_phi', ';DiMuon #phi;Events/0.064', 100, -3.2, 3.2), 'DiMuon_phi'),
        df.Histo1D(('DiMuon_m', ';DiMuon m_{#mu#mu} [GeV];Events/0.2 GeV', 100, 0, 20), 'DiMuon_m'),
    ]

    #df = df.Define('ZFinalMuMu', 'a_lead_decay == 1 ? AFinalLead : AFinalSubl') \
    #        .Define('ZFinalMuMu_decay_MuonsVec', 'a_lead_decay == 1 ? AFinalLead_decay_MuonsVec : AFinalSubl_decay_MuonsVec') \
    #        .Define('AFinalMuMu_decay_MuonsTLV', 'a_lead_decay == 1 ? AFinalLead_decay_MuonsTLV : AFinalSubl_decay_MuonsTLV') \
    #        .Define('a_2mu_dr', 'dRADecay({}, AFinalMuMu_decay_MuonsVec)') \
    #        .Define('a_2mu_pt', 'AFinalMuMu_decay_MuonsTLV.Pt() * 1e-3') \
    #        .Define('a_2mu_eta', 'AFinalMuMu_decay_MuonsTLV.Eta()') \
    #        .Define('a_2mu_phi', 'AFinalMuMu_decay_MuonsTLV.Phi()') \
    #        .Define('a_2mu_m', 'AFinalMuMu_decay_MuonsTLV.M() * 1e-3') \
    #        .Define('a_2mu_mus_pt', 'AFinalMuMu_decay_MuonsVec[0]->pt() * 1e-3') \
    #        .Define('a_2mu_mus_lead_pt', 'AFinalMuMu_decay_MuonsVec[0]->pt() * 1e-3') \
    #        .Define('a_2mu_mus_subl_pt', 'AFinalMuMu_decay_MuonsVec[1]->pt() * 1e-3') \
    #        .Define('a_2mu_lead_index', '(int)std::distance(TruthMuonsGoodOrderedVec.begin(), std::find(TruthMuonsGoodOrderedVec.begin(), TruthMuonsGoodOrderedVec.end(), AFinalMuMu_decay_MuonsVec[0]))')

    #hists += [
    #    df.Histo1D(('Z_2mu_dr', ';Z#rightarrow#mu#mu #DeltaR(#mu, #mu);Events', 500, 0, 1.5), 'Z_2mu_dr'),
    #    df.Histo1D(('Z_2mu_pt', ';Z#rightarrow#mu#mu p_{T}^{#mu#mu} [GeV];Events/3 GeV', 100, 0, 300), 'Z_2mu_pt'),
    #    df.Histo1D(('Z_2mu_eta', ';Z#rightarrow#mu#mu #eta_{#mu#mu};Events/0.06', 100, -3.0, 3.0), 'Z_2mu_eta'),
    #    df.Histo1D(('Z_2mu_phi', ';Z#rightarrow#mu#mu #phi_{#mu#mu};Events/0.064', 100, -3.2, 3.2), 'Z_2mu_phi'),
    #    df.Histo1D(('Z_2mu_m', ';Z#rightarrow#mu#mu m_{#mu#mu} [GeV];Events/0.25 GeV', 100, 0, 25), 'Z_2mu_m'),
    #    df.Histo1D(('Z_2mu_mus_pt', ';Z#rightarrow#mu#mu p_{T}^{#mu} [GeV];Events/3 GeV', 100, 0, 300), 'Z_2mu_mus_pt'),
    #    df.Histo1D(('Z_2mu_mus_lead_pt', ';Z#rightarrow#mu#mu p_{T}^{Lead #mu} [GeV];Events/3 GeV', 100, 0, 300), 'Z_2mu_mus_lead_pt'),
    #    df.Histo1D(('Z_2mu_mus_subl_pt', ';Z#rightarrow#mu#mu p_{T}^{Subl #mu} [GeV];Events/3 GeV', 100, 0, 300), 'Z_2mu_mus_subl_pt'),
    #    df.Histo1D(('Z_2mu_lead_index', ';Lead #mu index;Events', 20, 0, 20), 'Z_2mu_lead_index'),
    #]

    return df, hists
