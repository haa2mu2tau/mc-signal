import os, glob
from lib.utils import JOs_dir, get_JOs, DSID

# This file configures all the available samples for the MC generation test framework

def get_mass(d: int) -> float:
    jo_file = os.path.basename(list(glob.glob(os.path.join(JOs_dir, str(d), 'mc.*.py')))[0])[3:-3] # Remove 'mc.' and '.py'
    return float([t for t in jo_file.split('_') if t.startswith('ma')][0].removeprefix('ma').replace('p', '.')) # GeV

# Input LHE datasets
in_lhe_ggF = {
    2: 'mc15_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.evgen.TXT.e5607', # 2000 events per-file
    #3: 'mc23_13p6TeV.601485.Ph_PDF4LHC21_ggH125_MiNLO_LHE.evgen.TXT.e8537', # 2000 events per-file
    3: 'mc23_13p6TeV.601401.Ph_PDF4LHC21_VBFH125_LHE.evgen.TXT.e8537', # 11000 events per-file
}

in_lhe_VBF = {
    2: 'mc15_13TeV.345916.Powheg_NNPDF30_VBFH125_LHE.evgen.TXT.e6901', # 11000 events per-file
    3: 'mc23_13p6TeV.601401.Ph_PDF4LHC21_VBFH125_LHE.evgen.TXT.e8537', # 11000 events per-file
}

# Number of input files per DSID job
n_files = {
    603200: 40,
    603201: 40,
    603202: 50,
    603203: 50,
    603204: 50,
    603205: 50,
    603206: 50,

    603300: 40,
    603301: 40,
    603302: 50,
    603303: 50,
    603304: 50,
    603305: 50,
    603306: 50,
}

# Signal DSIDs groups
dsids = {
    'ana':          [DSID(d, f'm_{{a}} = {get_mass(d)} GeV', in_lhe_ggF, n_files[d], analysis='Haa2mu2tau') for d in get_JOs(603200, 603219)], # Initial request for analysis signals
    'ana-low-mass': [DSID(d, f'm_{{a}} = {get_mass(d)} GeV', in_lhe_ggF, analysis='Haa2mu2tau') for d in get_JOs(603220, 603223)], # Low-mass extension from 3.6 GeV to 3.9 GeV
    'ml-muhad':     [DSID(d, f'm_{{a}} = {get_mass(d)} GeV', in_lhe_ggF, analysis='Haa2mu2tau') for d in get_JOs(603250, 603259)], # ML training samples, with mu+had di-tau decay filter
    'ml-ehad':      [DSID(d, f'm_{{a}} = {get_mass(d)} GeV', in_lhe_ggF, analysis='Haa2mu2tau') for d in get_JOs(603260, 603269)], # ML training samples, with e+had di-tau decay filter
    'ml-hadhad':    [DSID(d, f'm_{{a}} = {get_mass(d)} GeV', in_lhe_ggF, analysis='Haa2mu2tau') for d in get_JOs(603270, 603279)], # ML training samples, with had+had di-tau decay filter

    'ana-VBF':      [DSID(d, f'm_{{a}} = {get_mass(d)} GeV', in_lhe_VBF, n_files[d], analysis='Haa2mu2tau') for d in get_JOs(603300, 603300+1)], # Initial request for analysis signals

    'Zmumu-vlm':    [DSID(790000, 'Slice 1', analysis='ZmumuVLM', input_DAOD_files={2:'/eos/user/w/weiming/Haa2mu2tau/DAOD_TRUTH3.Sh_2214_Zmumu_maxHTpTV2_Mll3p6_10_TruthDAOD.pool.root'})], # Standalone-produced Zmumu very-low-mass (3.6 GeV < m_{ll} < 10 GeV)
}
