import re

category_labels = {
    'test':         'H #rightarrow aa #rightarrow 2#mu + 2#tau', # test set
    'ana':          'H #rightarrow aa #rightarrow 2#mu + 2#tau', # Initial request for analysis signals
    'ana-low-mass': 'H #rightarrow aa #rightarrow 2#mu + 2#tau', # Low-mass extension from 3.6 GeV to 3.9 GeV
    'ml-muhad':     'H #rightarrow aa #rightarrow 2#mu + 2#tau, ML Training #tau_{#mu}#tau_{had}', # ML training samples, with mu+had di-tau decay filter
    'ml-ehad':      'H #rightarrow aa #rightarrow 2#mu + 2#tau, ML Training #tau_{e}#tau_{had}', # ML training samples, with e+had di-tau decay filter
    'ml-hadhad':    'H #rightarrow aa #rightarrow 2#mu + 2#tau, ML Training #tau_{had}#tau_{had}', # ML training samples, with had+had di-tau decay filter

    'Zmumu-vlm':    'Z #rightarrow #mu#mu, 3.6 GeV #leq m_{#mu#mu} #leq 10 GeV', # Very-low-mass Z->mumu samples
}

# Custom formatting options for hist_plotter.plotHist and plotHist2D,
# for each histogram name. Can use regexes!
plot_formats = {
    '.*': {'atlasLabel': 'Simulation Internal', 'legendColumns': 2},
    'a_(lead|subl)_decay': {'xAlphaBins': ['Unknown', '#mu#mu', '#tau_{#mu}#tau_{had}', '#tau_{e}#tau_{had}', '#tau_{had}#tau_{had}', '#tau_{lep}#tau_{lep}']},
    'a_2tau_decay': {'xAlphaBins': ['#tau_{#mu}#tau_{had}', '#tau_{e}#tau_{had}', '#tau_{had}#tau_{had}', '#tau_{lep}#tau_{lep}']},
}