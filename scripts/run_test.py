#!/usr/bin/env python
from typing import List, Dict, Optional, Any
import argparse, os, glob, shutil, datetime
from pathlib import Path
from copy import deepcopy

import ROOT

from lib.utils import ana_home, JOs_dir
from lib.truth_analysis import run_truth_analysis
from lib.hist_plotter import plot_directories

from config.plots import category_labels, plot_formats
from config.samples import DSID, dsids
all_dsids = {dsid_obj.dsid: dsid_obj for lst in dsids.values() for dsid_obj in lst}

def do_generate(outdir: str, workdir: str, inputdir: str, dsid: int, run: int, n_events: Optional[int] = None, seed: Optional[int] = None):
    if not os.getenv('AtlasVersion').startswith('23.6.'): raise RuntimeError('Invalid Athena version! Setup the correct environment with "source setup.sh -g"')

    curr_dir = os.getcwd()
    this_workdir = os.path.join(workdir, f'{dsid}_run{run}', 'gen')
    if os.path.exists(this_workdir):
        print(f'Warning! The work directory {this_workdir} already exists, and will be deleted')
        shutil.rmtree(this_workdir)
    Path(this_workdir).mkdir(parents=True)
    os.chdir(this_workdir)

    dsid_obj = all_dsids[dsid]
    input_files = list(glob.glob(os.path.join(inputdir, dsid_obj.input_LHE[run], '*TXT*'))) if dsid_obj.input_LHE else None
    if dsid_obj.n_files is not None: input_files = input_files[0:dsid_obj.n_files]

    out_file = f'output.{dsid}.EVNT.root'

    energy_per_run = {2: 13000, 3: 13600}

    cmd = f'Gen_tf.py --ecmEnergy={energy_per_run[run]} --outputEVNTFile={out_file} --jobConfig={os.path.join(JOs_dir, str(dsid))}'
    if input_files: cmd += ' --inputGeneratorFile=' + ','.join(input_files)
    if n_events: cmd += f' --maxEvents={n_events}'
    if seed: cmd += f' --randomSeed={seed}'
    os.system(cmd)

    this_outdir = os.path.join(outdir, f'{dsid}_run{run}', 'gen')
    Path(this_outdir).mkdir(parents=True, exist_ok=True)
    shutil.copy(out_file, this_outdir)
    shutil.copy('log.generate', this_outdir)

    os.chdir(curr_dir)
    shutil.rmtree(this_workdir)


def do_derivation(outdir: str, workdir: str, dsid: DSID, run: int, n_events: Optional[int] = None, derivation='TRUTH1'):
    if not os.getenv('AtlasVersion').startswith('24.0.'): raise RuntimeError('Invalid Athena version! Setup the correct environment with "source setup.sh -d"')

    curr_dir = os.getcwd()
    this_workdir = os.path.join(workdir, f'{dsid.dsid}_run{run}', 'derivation')
    if os.path.exists(this_workdir):
        print(f'Warning! The work directory {this_workdir} already exists, and will be deleted')
        shutil.rmtree(this_workdir)
    Path(this_workdir).mkdir(parents=True)
    os.chdir(this_workdir)

    if run in dsid.input_EVNT_files:
        evnt_file = os.path.realpath(dsid.input_EVNT_files[run])
    else:
        # Copy the input file locally to speedup the derivation
        evnt_file = f'output.{dsid.dsid}.EVNT.root'
        shutil.copy(os.path.join(outdir, f'{dsid.dsid}_run{run}', 'gen', evnt_file), evnt_file)

    out_file = f'output.{dsid.dsid}.pool.root'

    cmd = f'Derivation_tf.py --CA=True --formats={derivation} --outputDAODFile={out_file} --inputEVNTFile={evnt_file}'
    if n_events: cmd += f' --maxEvents={n_events}'
    os.system(cmd)

    out_file = f'DAOD_{derivation}.{out_file}' # Derivation_tf adds a prefix to the output filename

    this_outdir = os.path.join(outdir, f'{dsid.dsid}_run{run}', 'derivation')
    Path(this_outdir).mkdir(parents=True, exist_ok=True)
    shutil.copy(out_file, this_outdir)
    shutil.copy('log.Derivation', this_outdir)

    os.chdir(curr_dir)
    shutil.rmtree(this_workdir)


def do_tarball(outdir: str, workdir: str, dsids: List[int], run: int):
    curr_dir = os.getcwd()
    this_workdir = os.path.join(workdir, f'tarball_run{run}')
    if os.path.exists(this_workdir):
        print(f'Warning! The work directory {this_workdir} already exists, and will be deleted')
        shutil.rmtree(this_workdir)
    Path(this_workdir).mkdir(parents=True)
    os.chdir(this_workdir)

    # Copy the JOs and Gen_tf log
    for dsid in dsids:
        shutil.copytree(os.path.join(JOs_dir, str(dsid)), str(dsid), symlinks=True) # Preserve the symlink structure
        shutil.copy(os.path.join(outdir, f'{dsid}_run{run}', 'gen/log.generate'), str(dsid))

    out_file = f'tarball_run{run}_{datetime.datetime.now().strftime("%Y%m%d-%H%M%S")}.tar.gz'
    os.system(f'tar -zcvf {os.path.join(outdir, out_file)} .')

    os.chdir(curr_dir)
    shutil.rmtree(this_workdir)


def do_analysis(outdir: str, workdir: str, dsids: List[DSID], category: str, run: int, truth_analysis: str, formats: Dict[str, Dict[str, Any]]):
    if not os.getenv('AtlasVersion').startswith('24.2.'): raise RuntimeError('Invalid Athena version! Setup the correct environment with "source setup.sh -v"')

    curr_dir = os.getcwd()
    plot_dirname = f'plots_{category}_run{run}'
    this_workdir = os.path.join(workdir, plot_dirname)
    if os.path.exists(this_workdir):
        print(f'Warning! The work directory {this_workdir} already exists, and will be deleted')
        shutil.rmtree(this_workdir)
    Path(this_workdir).mkdir(parents=True)
    os.chdir(this_workdir)

    out_file = f'plots_{category}_run{run}.root'

    # Load the truth analysis
    for dsid in dsids:
        print(f'>>>> - DSID: {dsid.dsid}, {dsid.name}')

        if run in dsid.input_DAOD_files:
            daod_file = os.path.realpath(dsid.input_DAOD_files[run])
        else:
            # Copy the input file locally to speedup the derivation
            daod_file = list(glob.glob(os.path.join(outdir, f'{dsid.dsid}_run{run}/derivation', f'DAOD_TRUTH*.output.{dsid.dsid}.pool.root')))[0]
            # Copy the input file locally to speedup the derivation
            shutil.copy(daod_file, os.path.basename(daod_file))

        run_truth_analysis(daod_file, out_file, dsid.dsid, run, truth_analysis)

    # Load up all directories
    file = ROOT.TFile.Open(out_file, 'READ')
    input_dirs = [file.Get(key.GetName()) for key in file.GetListOfKeys()]
    dir_labels = {f'{d.dsid}_run{run}': d.name for d in dsids}

    if run == 2: energy = '13'
    elif run == 3: energy = '13.6'
    elif run == 4: energy = '14'
    format_override = {
        'energy': f'{energy} TeV',
        'optionalLabel': category_labels[category] if category in category_labels else category,
    }

    plot_directories(input_dirs, this_workdir, dir_labels, formats, format_override)

    shutil.copytree(
        this_workdir, 
        os.path.join(outdir, f'{plot_dirname}_{datetime.datetime.now().strftime("%Y%m%d-%H%M%S")}'),
        dirs_exist_ok=True,
        ignore=shutil.ignore_patterns('DAOD_TRUTH*')
    )

    os.chdir(curr_dir)
    shutil.rmtree(this_workdir)


def main(argv=None):
    parser = argparse.ArgumentParser(description='Local test of the Signal production for the H->aa->2mu2tau analysis')
    parser.add_argument('-o', '--output-dir', default='test_output', help='Work directory where the final output files will be located')
    parser.add_argument('-i', '--input-dir', default=os.path.join(ana_home, 'input'), help='Input directory where the mc* folders with LHE files downloaded from Rucio are located')
    parser.add_argument('-w', '--work-dir', default='tmp', help='Work directory where the tmp output files will be located')
    parser.add_argument('-f', '--input_files', nargs='+', help='Input files override for the derivation or analysis steps (can only run 1 step/run at a time!)')

    DSIDs_grp = parser.add_argument_group('DSIDs')
    DSIDs_grp.add_argument('--dsid', nargs='+', choices=list(dsids.keys()) + sorted(str(d) for d in all_dsids), help='DSID or DSID-category list to run on, in the JOs directory')
    
    opts_grp = parser.add_argument_group('Options')
    opts_grp.add_argument('-r', '--run', type=int, nargs='+', choices=[2, 3], default=[2], help='LHC Run (2 or 3)')
    opts_grp.add_argument('-n', '--n-events', type=int, help='Number of output events (be careful! if the efficiencies are too low, the event generation will produce an error if there are not enough input files)')
    opts_grp.add_argument('--seed', type=int, help='Random seed, used for the event generation')
    
    steps_grp = parser.add_argument_group('Steps')
    steps_grp.add_argument('-g', '--generate', action='store_true', help='Run Gen_tf.py on the selected DSIDs for the selected DSIDs')
    steps_grp.add_argument('-d', '--derivation', nargs='?', const='TRUTH1', choices=[False, 'TRUTH0', 'TRUTH1', 'TRUTH2', 'TRUTH3'], help='Run Derivation_tf.py and produce DAOD_TRUTH0/1/2/3 files for the selected DSIDs; by default, DAOD_TRUTH1 is produced')
    steps_grp.add_argument('-a', '--analysis', nargs='?', const='auto', help='Run the validation truth-analysis and produce plots for the MC request. By default, the analysis configured in the DAOD definition is used, unless overriden in this argument')
    steps_grp.add_argument('-t', '--tarball', action='store_true', help='Produce the tarball for the MC request')

    args = parser.parse_args(argv)

    # Safeguard against running on different Athena releases:
    if args.generate and args.derivation:
        raise RuntimeError('Cannot run with both --generate and --derivation at the same time, they use different Athena releases!')

    # Get absolute paths, for when we change the current directory:
    args.output_dir = os.path.realpath(args.output_dir)
    args.input_dir = os.path.realpath(args.input_dir)
    args.work_dir = os.path.realpath(args.work_dir)

    # Create directories
    Path(args.output_dir).mkdir(parents=True, exist_ok=True)
    Path(args.work_dir).mkdir(parents=True, exist_ok=True)
    
    this_dsids = {dsid_obj.dsid for grp in args.dsid if grp in dsids for dsid_obj in dsids[grp]} | {int(d) for d in args.dsid if d not in dsids} if args.dsid else set()

    if args.input_files:
        if args.generate:
            raise RuntimeError('Cannot specify --input-files for the --generate step; use the --input-dir arguement instead!')
        if args.tarball:
            raise RuntimeError('Cannot specify --input-files for the --tarball step')
        if args.derivation and args.analysis:
            raise RuntimeError('Cannot run over two steps if specifying --input-files')
        if len(args.run) > 1:
            raise RuntimeError('Cannot run over more than 1 run at a time if specifying --input-files')
        args.input_files = [os.path.realpath(f) for f in args.input_files]
        if len(args.input_files) != len(this_dsids):
            raise RuntimeError('You have to specify one input file for each selected DSID if running with --input-files')

    for run in args.run:
        for i, dsid in enumerate(this_dsids):
            dsid_obj = all_dsids[dsid]
            if args.generate:
                print(f'>>>> Generating DSID {dsid}: {dsid_obj.name}')
                do_generate(args.output_dir, args.work_dir, args.input_dir, dsid, run, args.n_events, args.seed)

            if args.derivation:
                if args.input_files:
                    dsid_obj = deepcopy(dsid_obj)
                    dsid_obj.input_EVNT_files[run] = args.input_files[i]

                print(f'>>>> Running TRUTH1/2/3 derivation on DSID {dsid}: {dsid_obj.name}')
                do_derivation(args.output_dir, args.work_dir, dsid_obj, run, args.n_events, args.derivation, input_file=input_file)

        if args.analysis:
            grps = [grp for grp in args.dsid if grp in dsids]
            others = [all_dsids[int(d)] for d in args.dsid if d not in grps]
            if others: grps.append('others')

            already_used_files = 0
            for grp in grps:
                grp_dsids = others if grp == 'others' else dsids[grp]

                if args.input_files:
                    input_files = args.input_files[already_used_files:already_used_files+len(grp_dsids)]
                    grp_dsids = deepcopy(grp_dsids)
                    for file, dsid in zip(input_files, grp_dsids):
                        dsid.input_DAOD_files[args.run[0]] = file # Only one run per execution with --input-files
                    already_used_files += len(grp_dsids)

                if args.analysis != 'auto':
                    analysis = args.analysis
                else:
                    all_analyses = list(set(dsid.analysis for dsid in grp_dsids))
                    if len(all_analyses) > 1:
                        raise RuntimeError('Cannot execute multiple analysis at once for the same group of samples!')
                    if all_analyses[0] == None:
                        raise RuntimeError('You have to specify an analysis to run on the selected samples!')
                    analysis = all_analyses[0]

                print(f'>>>> Running truth analysis "{analysis}" for DSID group "{grp}"')

                do_analysis(args.output_dir, args.work_dir, grp_dsids, grp, run, analysis, plot_formats)

        if args.tarball:
            print(f'>>>> Preparing MC request tarball for DSIDs: {this_dsids}')
            do_tarball(args.output_dir, args.work_dir, this_dsids, run)

if __name__ == '__main__':
    main()
