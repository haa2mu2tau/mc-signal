# H -> aa -> 2mu 2tau MC signal samples

The MC Signal generation for the analysis relies on LHE input files from the ggF SM Higgs production, generated with Powheg. The LHE files are [stored centrally](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HLRSGroup#MC_Information_For_Run_3):

- MC23 (Run 3): `mc23_13p6TeV.601485.Ph_PDF4LHC21_ggH125_MiNLO_LHE.evgen.TXT.e8537`, with 2000 events per LHE file
- MC20 (Run 2): `mc15_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.evgen.TXT.e5607`, with 2000 events per LHE file

The LHE files are used as input to Pythia 8, which runs the parton shower for the process $`H \to aa \to 2\mu + 2\tau`$. This is implemented in the analysis-specific JOs.

## Filters

To reduce the size of the signal samples, two or three harmonized filters are applied

1. `LepFourFilter` (`MultiElecMuTauFilter`): Pass if we have at least 4 total leptons (including hadronically-decaying taus)
2. `TauTwoFilter` (`TauFilter`): Pass if the event has at 2 taus (including hadronically-decaying taus)
3. `TauChannelFilter` (`TauFilter`): Optional. Selects between the semileptonic $`a \to \tau_\mu \tau_{\text{had}}`$ and $`a \to \tau_e \tau_{\text{had}}`$ channels, or the $`a \to \tau_{\text{had}} \tau_{\text{had}}`$ fully-hadronic channel.

All electrons and muons are required to pass $`p_T > 5 \text{GeV}`$. Hadronically-decaying tau leptons must have a visible minimum transverse momentum of $`p_T > 10 \text{GeV}`$. The particles must also pass a baseline $`|\eta| < 3`$ selection.

`TauChannelFilter` allows to select a specific final state decay channel, to produce the ML event tagger training samples.

## DSIDs
Preliminarily, the following inclusive analysis signal DSIDs are defined:
- 603200: $`m_a = 4.0 \ \text{GeV}`$.
- 603201: $`m_a = 6.0 \ \text{GeV}`$.
- 603202: $`m_a = 8.0 \ \text{GeV}`$.
- 603203: $`m_a = 12.0 \ \text{GeV}`$.
- 603204: $`m_a = 14.0 \ \text{GeV}`$.
- 603205: $`m_a = 16.0 \ \text{GeV}`$.
- 603206: $`m_a = 20.0 \ \text{GeV}`$.

Additional inclusive analysis signal DSIDs are defined for additional mass-points:
- 603220: $`m_a = 3.6 \ \text{GeV}`$.
- 603221: $`m_a = 3.7 \ \text{GeV}`$.
- 603222: $`m_a = 3.8 \ \text{GeV}`$.
- 603223: $`m_a = 3.9 \ \text{GeV}`$.

Additional DSIDs to produce training samples for the event tagger, filtered by channel:
- 603250: $`m_a = 4.0 \ \text{GeV}`$, $`a \to \tau_\mu \tau_{\text{had}}`$ channel.
- 603251: $`m_a = 8.0 \ \text{GeV}`$, $`a \to \tau_\mu \tau_{\text{had}}`$ channel.
- 603252: $`m_a = 14.0 \ \text{GeV}`$, $`a \to \tau_\mu \tau_{\text{had}}`$ channel.
- 603260: $`m_a = 4.0 \ \text{GeV}`$, $`a \to \tau_e \tau_{\text{had}}`$ channel.
- 603261: $`m_a = 8.0 \ \text{GeV}`$, $`a \to \tau_e \tau_{\text{had}}`$ channel.
- 603262: $`m_a = 14.0 \ \text{GeV}`$, $`a \to \tau_e \tau_{\text{had}}`$ channel.
- 603270: $`m_a = 4.0 \ \text{GeV}`$, $`a \to \tau_{\text{had}} \tau_{\text{had}}`$ channel.
- 603271: $`m_a = 8.0 \ \text{GeV}`$, $`a \to \tau_{\text{had}} \tau_{\text{had}}`$ channel.
- 603272: $`m_a = 14.0 \ \text{GeV}`$, $`a \to \tau_{\text{had}} \tau_{\text{had}}`$ channel.

Additional DSID to use with Release 21 (`asetup AthGeneration,21.6.106`):
- 699999: $`m_a = 4.0 \text{GeV}`$.



## Instructions

### Signal generation

To test-run the MC signal genereation, first download the input files from Rucio. By default, the script will try to locate them in the `input` directory:

	cd input
	rucio download --nrandom 100 mc23_13p6TeV.601485.Ph_PDF4LHC21_ggH125_MiNLO_LHE.evgen.TXT.e8537 mc15_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.evgen.TXT.e5607

Setup your release 23 AthGeneration environment in EL9 with

	source setup.sh -g

Launch the automated EVNT generation test with the `run_test.py` script

	run_test.py -o test_output -r 3 --dsid ana -g

The tarball required to submit the MC production request can be automatically produced after running the generation test on all DSIDs with the `-t` option. Tables with filter efficiencies, request size, and other generation information can be created from the jobs' logs with `scripts/gen_analysis.ipynb`.

You can manually run the event generation with

	Gen_tf.py --ecmEnergy=13600.0 --randomSeed=1234 --jobConfig path/to/JOs/603200 --outputEVNTFile=output.EVNT.root --maxEvents=10 --inputGeneratorFile path/to/LHE/files/TXT.<some-numbers>._<some-more-numbers>.tar.gz.1

### TRUTH1 derivation

The Athena derivation framework runs on release 24, and can be setup on any EL9 node with

	source setup.sh -d

Launch the `TRUTH1` derivation production with

	run_test.py -o test_output -r 3 --dsid ana -d

Alternatively, the `TRUTH0/2/3` derivations can be used, indicated with e.g. `-d TRUTH3`.

By default, the input EVNT file for each DSID is loaded from the output directory, which is expected to contain the propper folder structure from the output of the previous generation step. This can be overriden either in the `DSID` config in `config/samples.py` (with the `input_EVNT_files` dictionary, indicating **one** input EVNT file per-run, e.g. `input_EVNT_files={2: 'run.2.file.root', 3: 'run.3.file.root'}`), or with the `-f` command line argument, once again indicating **one** EVNT file per-DSID (if running over multiple DSIDs) one run at a time.

### Truth analysis and validation plots

The truth analysis and validation plot-production scripts run on AnalysisBase 24, and can be setup on an EL9 node with

	source setup.sh -a

The truth analysis is based on `RDataFrame`, and is defined in `config/analysis/<analysis-name>.py`. Helper functions are defined in the C++ header `config/analysis/helper_functions.h`, loaded by default during initialization. To run the analysis, after generating the EVNT files and the DAOD_TRUTH1/2/3 following the instructions above, execute

	run_test.py -o test_output -r 3 --dsids ana -a

By default, the analysis indicated for each `DSID` in `config/samples.py` is run (all DSIDs to be plotted at the same time MUST use the same analysis!). This can be overriden, with e.g. `-a <analysis-name>`.

The script will run the analysis on all DSIDs in the dsid-group (or the individual DSIDs specified), and then it will produce all plots with all DSIDs in that group.

By default, the input DAOD file for each DSID is loaded from the output directory, which is expected to contain the propper folder structure from the output of the previous derivation step. This can be overriden either in the `DSID` config in `config/samples.py` (with the `input_DAOD_files` dictionary, indicating **one** input DAOD file per-run, e.g. `input_DAOD_files={2: 'run.2.file.root', 3: 'run.3.file.root'}`), or with the `-f` command line argument, once again indicating **one** DAOD file per-DSID (if running over multiple DSIDs) one run at a time.