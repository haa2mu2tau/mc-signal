#--------------------------------------------------------------
# Use LHE files as input
#--------------------------------------------------------------

#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )
    

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

A_Mass = 4.0    # GeV
H_Mass = 125.0  # GeV
H_Width = 0.00407
A_Width = ( float(A_Mass) / 100. ) * 0.1 #100 MeV width for 100 GeV a 
A_MassMin = float(A_Mass) - 100*float(A_Width)
A_MassMax = float(A_Mass) + 100*float(A_Width)
genSeq.Pythia8.Commands += [
  'Higgs:useBSM = on',
  '35:m0 = '+str(H_Mass),
  '35:mWidth = '+str(H_Width),
  '35:doForceWidth = off',
  '35:onMode = off',
  '35:oneChannel = 1 1 100 36 36',  # h->aa
  '36:onMode = off',       
  '36:addChannel = on 0.5 100 13 -13',     # a -> mumu
  '36:addChannel = on 0.5 100 15 -15',     # a -> tautau 
  '36:m0 = '+str(A_Mass),      #scalar mass
  '36:mWidth = '+str(A_Width), # narrow width
  '36:mMin = '+str(A_MassMin), # narrow width
  '36:mMax = '+str(A_MassMax)  # narrow width
  ]

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("LepFourFilter")
filtSeq.LepFourFilter.IncludeHadTaus = True
filtSeq.LepFourFilter.NLeptons = 4
filtSeq.LepFourFilter.MinPt = 5000
filtSeq.LepFourFilter.MinVisPtHadTau = 10000
filtSeq.LepFourFilter.MaxEta = 3

from GeneratorFilters.GeneratorFiltersConf import TauFilter
filtSeq += TauFilter("TauTwoFilter")
filtSeq.TauTwoFilter.Ntaus = 2
filtSeq.TauTwoFilter.EtaMaxe = 3
filtSeq.TauTwoFilter.EtaMaxmu = 3
filtSeq.TauTwoFilter.EtaMaxhad = 3
filtSeq.TauTwoFilter.Ptcute = 5000
filtSeq.TauTwoFilter.Ptcutmu = 5000
filtSeq.TauTwoFilter.Ptcuthad = 10000

filtSeq += TauFilter("TauThreeFilter")
filtSeq.TauThreeFilter.Ntaus = 3
filtSeq.TauThreeFilter.EtaMaxe = 3
filtSeq.TauThreeFilter.EtaMaxmu = 3
filtSeq.TauThreeFilter.EtaMaxhad = 3
filtSeq.TauThreeFilter.Ptcute = 5000
filtSeq.TauThreeFilter.Ptcutmu = 5000
filtSeq.TauThreeFilter.Ptcuthad = 10000

filtSeq.Expression = "LepFourFilter and TauTwoFilter and not TauThreeFilter"

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 5 # Multiple external LHE files as inputs
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, ggH H->aa->2mu2tau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","BSMHiggs", "mH125" ]
evgenConfig.contact     = [ 'oducu@cern.ch and Alexandra.Tudorache@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "Powheg", "EvtGen" ]

