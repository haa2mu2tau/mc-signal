#!/bin/bash

usage_fn ()
{
    echo "usage: setup.sh [-h] [-g mc20,mc23] [-d] [-p]"
}

help_fn ()
{
    echo "$MC signal production setup tool."
    echo ""
    echo "options:"
    echo "  -h, --help            show this help message and exit"
    echo "  -g mc20 | mc23        setup AthGeneration for mc20 or mc23"
    echo "  -d                    setup Athena on el9, for producing DAOD_TRUTH1 derived files"
    echo "  -a                    setup AnalysisBase on el9, for validation truth-analysis and plots"
}

setup="none"
campaign=""

# Parse cmd line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -g)
            setup="gen"
            campaign=$2
            shift
            if [[ $campaign != "mc20" ]] && [[ $campaign != "mc23" ]]; then
                echo "You have to specify the generation campaign: mc20 o mc23"
                echo ""
                usage_fn
                return 1
            fi
            ;;
        -d) setup="derivation" ;;
        -a) setup="analysis" ;;
        -h|--help)
            usage_fn
            echo ""
            help_fn
            ;;
        *)
            usage_fn
            echo "setup.sh: error: Invalid option (see them with -h)"
            return 1
    esac
    shift
done

VERSION_ID=$(awk -F= '$1=="VERSION_ID" {gsub(/"/, "", $2); print $2}' /etc/os-release)
MAJOR_VERSION_ID=$(echo $VERSION_ID | cut -d. -f1)
#if [[ $MAJOR_VERSION_ID != '9' ]] && { [[ $MAJOR_VERSION_ID != '7' ]] || [[ $campaign != 'mc20' ]]; }; then
#    echo "You need to be in a EL9 environment, or CentOS 7 if generating events for mc20"
#    return 1
#fi

echo "Setting up environment..."

# Platform-specific setup
if [[ $setup == "none" ]]; then
    echo "Specify the Athena configuration: -g for AthGeneration, -d to produce DAOD_TRUTH with Athena, or -a to run the truth-analysis and produce the validation plots"
elif command -v setupATLAS &> /dev/null || command -v asetup &> /dev/null; then
    if ! command -v asetup &> /dev/null; then
        setupATLAS
    fi

    if [[ $setup == 'gen' ]]; then
        #if [[ $campaign == 'mc23' ]]; then asetup AthGeneration,23.6.28 # HepMC 3
        if [[ $campaign == 'mc23' ]]; then asetup AthGeneration,23.6.37 # HepMC 3
        elif [[ $campaign == 'mc20' ]]; then asetup AthGeneration,23.6.21 # HepMC 2
        else echo "Invalid MC campaign"; return 1; fi
    elif [[ $setup == 'derivation' ]]; then asetup Athena,24.0.60
    elif [[ $setup == 'analysis' ]]; then asetup AnalysisBase,24.2.37
    else echo "Invalid setup configuration"; return 1; fi
else
    echo "You're not in a supported environment (LXPlus with ATLAS' software setup). You're responsible for ensuring you have the right configuration!"
fi

export ANA_HOME=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
export PATH="$ANA_HOME/scripts:$PATH"
export PYTHONPATH="$ANA_HOME:$PYTHONPATH"
