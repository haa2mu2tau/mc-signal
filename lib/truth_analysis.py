import os
from lib.utils import ana_home

import ROOT

__is_initialized = False

def run_truth_analysis(daod_truth_file: str, outfile: str, dsid: int, run: int, truth_analysis: str):
    global __is_initialized
    if not __is_initialized: 
        ROOT.EnableImplicitMT()
        ROOT.xAOD.Init().ignore()
 
        ROOT.xAOD.ROOT6_xAODDataSource_WorkAround_Dummy()

        # Compile helper functions
        directory = os.path.dirname(__file__)
        ROOT.gInterpreter.ProcessLine(f'#include "{ana_home}/config/analysis/helper_functions.h"')

        __is_initialized = True

    # Create a dataframe object.
    df_orig = ROOT.xAOD.MakeDataFrame(daod_truth_file, 'CollectionTree', False)

    from importlib import import_module
    try:
        truth_ana_module = import_module(f'config.analysis.{truth_analysis}')
    except:
        raise RuntimeError(f'Could not import the truth analysis config file config/analysis/{truth_analysis}.py')
    df, hists = truth_ana_module.run_analysis(df_orig, dsid, run)
    
    # Run event loop and save the output to a ROOT file
    print('Running event loop...')
    f_out = ROOT.TFile.Open(outfile, 'UPDATE')
    out_dirname = f'{dsid}_run{run}'
    if not f_out.Get(out_dirname): f_out.mkdir(out_dirname)
    out_dir = f_out.Get(out_dirname)
    out_dir.cd()
    for h in hists: h.SetDirectory(out_dir)
    f_out.Write('', ROOT.TObject.kOverwrite)
    f_out.Close()

    # Cutflow
    print('Truth selection cutflow:')
    report = df.Report()
    report.Print()
    print()
