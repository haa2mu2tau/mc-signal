from typing import Optional, Union, List
import ctypes

import ROOT

def root_plot_setup() -> None:
    from . import ATLASstyle
    ATLASstyle.set_atlas_style()
    ROOT.gROOT.SetBatch(ROOT.kTRUE) # No graphical output


#FROM ATLASPLOTS (github, since pip version has no 'norm' option)
def hist_to_graph(
    hist: ROOT.TH1, 
    bin_err: Union[None, str, ROOT.TH1.EBinErrorOpt] = None, 
    show_bin_width: bool = False, 
    norm: bool = False, 
    remove_zero: bool = False
) -> ROOT.TGraph:
    """Convert histogram (TH1) to graph (TGraph).
    Parameters
    ----------
    hist : ROOT TH1
        The ROOT histogram.
    bin_err : None, str, TH1.EBinErrorOpt, optional
        Type of bin error to use. Choose from the following options:
        - 'none': use the bin-error option already defined in `hist`. This is
          the default option.
        - 'normal' or `ROOT.TH1.EBinErrorOpt.kNormal`: errors with Normal (Wald)
          approximation: errorUp = errorLow = sqrt(N).
        - 'poisson' or `ROOT.TH1.EBinErrorOpt.kPoisson`: errors from Poisson
          interval at 68.3% (1 sigma).
        - 'poisson2' or `ROOT.TH1.EBinErrorOpt.kPoisson2`: errors from Poisson
          interval at 95% CL (~2 sigma).
        For more details, see `TH1.EBinErrorOpt`_.
        .. _TH1.EBinErrorOpt: \
            https://ROOT.cern.ch/doc/master/classTH1.html#ac6e38c12259ab72c0d574614ee5a61c7
    show_bin_width : bool, optional
        If True, use graph x error bars to show bin width and place marker at
        centre of bin. The x error bars are set to 0 by default.
    norm : bool, optional
        Normalize the resulting graph such that the y value of each point is 1 and the
        error is y_err/y. The x values and their errors remain the same. This is useful
        for showing the relative error of a histogram in a ratio plot, for example.
    remove_zero : bool, optional
        If True, do not include bins of the original hist where the value is 0, regardless
        of the error.
    Returns
    -------
    ROOT TGraphAsymmErrors:
        TGraph equivalent of `hist`.
    """
    if bin_err is not None:
        # Make copy of histogram since we'll need to modify its bin error option
        tmp_hist = hist.Clone(hist.GetName() + "_tmp")
    else:
        tmp_hist = hist

    # Convert to and return as graph
    graph = ROOT.TGraphAsymmErrors(tmp_hist.GetNbinsX())

    # Recall in ROOT, the first bin has index 1 (bin 0 is underflow)!
    for i_bin in range(1, tmp_hist.GetNbinsX()+1):
        N = tmp_hist.GetBinContent(i_bin)

        if remove_zero and N == 0:
            continue

        if not norm:
            graph.SetPoint(i_bin-1, tmp_hist.GetBinCenter(i_bin), N)
        else:
            graph.SetPoint(i_bin-1, tmp_hist.GetBinCenter(i_bin), 1.)

        # Get errors
        if bin_err == None:
            pass
        elif bin_err == "normal" or bin_err == ROOT.TH1.EBinErrorOpt.kNormal:
            tmp_hist.SetBinErrorOption(ROOT.TH1.EBinErrorOpt.kNormal)
        elif bin_err == "poisson" or bin_err == ROOT.TH1.EBinErrorOpt.kPoisson:
            tmp_hist.SetBinErrorOption(ROOT.TH1.EBinErrorOpt.kPoisson)
        elif bin_err == "poisson2" or bin_err == ROOT.TH1.EBinErrorOpt.kPoisson2:
            tmp_hist.SetBinErrorOption(ROOT.TH1.EBinErrorOpt.kPoisson2)
        else:
            raise ValueError("unknown bin error option '{}'".format(bin_err))

        if not norm:
            y_err_lo = tmp_hist.GetBinErrorLow(i_bin)
            y_err_up = tmp_hist.GetBinErrorUp(i_bin)
        else:
            if N == 0:
                continue
            y_err_lo = tmp_hist.GetBinErrorLow(i_bin) / N
            y_err_up = tmp_hist.GetBinErrorUp(i_bin) / N

        if show_bin_width:
            # Use x error bars to show bin width
            x_err_lo = tmp_hist.GetBinWidth(i_bin)/2
            x_err_up = x_err_lo
        else:
            # No x error bars
            x_err_lo = 0.
            x_err_up = 0.

        graph.SetPointError(i_bin-1, x_err_lo, x_err_up, y_err_lo, y_err_up)

    if bin_err != "none":
        # Delete the clone of `hist`
        del tmp_hist

    return graph



def autoscalePlot(
    parentObject: ROOT.TObject,
    plottedObjects: List[ROOT.TObject],
    avoidLabels: List[ROOT.TText] = [],
    avoidBoxes: List[ROOT.Rectangle_t] = [],
    avoidLegend: Optional[ROOT.TLegend] = None,
    expandAvoidToLeft: bool = False,
    expandAvoidToRight: bool = False,
    useHistErrors: bool = False,
    separationMargin: float = 0.01,
) -> None:
    '''
    Parameters:
        parentObject : ROOT.TObject
            Main plot object which owns the TAxis. Not used in the autoscale
        plottedObjects : [ROOT.TObject]
            Plotted objects (ROOT.TH1 or ROOT.TGraph) that will be used 
            in the autoscale.
        avoidLabels : [ROOT.TText]
            TText (or TLatex) labels that we want to avoid. Labels should be
            bottom-vertical-aligned.
        avoidBoxes : [ROOT.Rectangle_t]
            Bounding boxes of other objects that we want to avoid, like TLegend
        avoidLegend: ROOT.TLegend or None
            Temporary fix, since legend.GetBBox() is broken
        expandAvoidToLeft : bool
            Expands the avoid-zone from the left-most element to the left edge 
            of the plot
        expandAvoidToRight : bool
            Expands the avoid-zone from the right-most element to the right edge 
            of the plot
        useHistErrors: bool
            Use the histograms errors as part of the plot's maximums calculation
    '''
    # If we don't update and we are using log scale, it screws up with the user y coordinates
    ROOT.gPad.Update()

    # x in user coords, y in NDC
    avoidLabels_mixedCoords = []
    for label in avoidLabels:
        w = ctypes.c_uint()
        h = ctypes.c_uint()
        
        label.GetBoundingBox(w, h)

        x0 = ROOT.gPad.AbsPixeltoX(ROOT.gPad.UtoPixel(label.GetX()))
        x1 = ROOT.gPad.AbsPixeltoX(ROOT.gPad.UtoPixel(label.GetX()) + w.value)
        y0 = label.GetY() - separationMargin # Add some margin below the label
        y1 = label.GetY() # Broken, but doesn't matter
        avoidLabels_mixedCoords.append((x0, x1, y0, y1))

        cBoxX = (ctypes.c_int*4)()
        cBoxY = (ctypes.c_int*4)()
        label.GetControlBox(
            ROOT.gPad.UtoAbsPixel(label.GetX()),
            ROOT.gPad.VtoAbsPixel(label.GetY()),
            0,
            cBoxX,
            cBoxY
        )

        #print(ROOT.gPad.AbsPixeltoX(cBoxX[0]), ROOT.gPad.AbsPixeltoX(cBoxX[2]))
        
        #print(x0, x1, ROOT.gPad.VtoPixel(y0), ROOT.gPad.VtoPixel(y1))

        #bbox = label.GetBBox()
        #print(ROOT.gPad.UtoPixel(label.GetX()), bbox.fX)
        #print(ROOT.gPad.VtoPixel(label.GetY()), bbox.fY)
        #print(w.value, bbox.fWidth, '---', h.value, bbox.fHeight)

        #print('')

        
    # x in user coords, y in NDC
    avoidBoxes_mixedCoords = []
    for box in avoidBoxes:
        x0 = ROOT.gPad.PixeltoX(box.fX)
        x1 = ROOT.gPad.PixeltoX(box.fX + box.fWidth)
        y0 = (ROOT.gPad.GetWh() - (box.fY + box.fHeight)) / ROOT.gPad.GetWh() - separationMargin # Add some margin
        y1 = (ROOT.gPad.GetWh() - box.fY) / ROOT.gPad.GetWh()
        avoidBoxes_mixedCoords.append((x0, x1, y0, y1))

    avoidLegend_mixedCoords = []
    if avoidLegend is not None:
        avoidLegend_mixedCoords.append((
            avoidLegend.GetX1(),
            avoidLegend.GetX2(),
            avoidLegend.GetY1NDC(),
            avoidLegend.GetY2NDC()
        ))

    avoid_mixedCoords = avoidBoxes_mixedCoords + avoidLabels_mixedCoords + avoidLegend_mixedCoords

    xmin, xmax = ROOT.gPad.GetUxmin(), ROOT.gPad.GetUxmax()
    if expandAvoidToLeft:
        lowestX = xmax
        lowestX_y = float('+inf')
        for avoid in avoid_mixedCoords:
            if avoid[0] <= lowestX:
                # Pick the left-most lowest one 
                if avoid[0] < lowestX:
                    lowestX_y = avoid[2]
                else:
                    lowestX_y = min(lowestX_y, avoid[2])

                lowestX = avoid[0]
        avoid_mixedCoords.append((xmin, lowestX, lowestX_y, lowestX_y))
    if expandAvoidToRight:
        highestX = xmin
        highestX_y = float('+inf')
        for avoid in avoid_mixedCoords:
            if avoid[1] >= highestX:
                # Pick the right-most lowest one 
                if avoid[1] > highestX:
                    highestX_y = avoid[2]
                else:
                    highestX_y = min(highestX_y, avoid[2])

                highestX = avoid[1]
        avoid_mixedCoords.append((highestX, xmax, highestX_y, highestX_y))

    plottedMaximums = []
    for obj in plottedObjects:
        if isinstance(obj, ROOT.TH1):
            num = obj.GetNbinsX()
            for bin in range(1, num + 1):
                point_xmin = obj.GetBinLowEdge(bin)
                point_xmax = obj.GetBinLowEdge(bin) + obj.GetBinWidth(bin)
                point_ymax = obj.GetBinContent(bin)
                point_ymax += obj.GetBinErrorUp(bin) if useHistErrors else 0
                plottedMaximums.append((point_xmin, point_xmax, point_ymax))
        elif isinstance(obj, ROOT.TGraph):
            num = obj.GetN()
            for i in range(0, num):
                point_xmin = obj.GetPointX(i) - obj.GetErrorXlow(i)
                point_xmax = obj.GetPointX(i) + obj.GetErrorXhigh(i)
                point_ymax = obj.GetPointY(i) + obj.GetErrorYhigh(i)
                plottedMaximums.append((point_xmin, point_xmax, point_ymax))

    logy = ROOT.gPad.GetLogy()

    #print(avoid_mixedCoords)
    #print('')
    #print(plottedMaximums)
    #print('')

    def isOverlapped() -> bool:
        for point in plottedMaximums:
            for avoid in avoid_mixedCoords:
                if point[1] < avoid[0] or point[0] > avoid[1]:
                    continue

                avoid_min_user = ROOT.gPad.PixeltoY(ROOT.gPad.VtoPixel(avoid[2]) - int(ROOT.gPad.GetWh() * ROOT.gPad.GetHNDC()))
                avoid_min_user = 10**(avoid_min_user) if logy else avoid_min_user
                #print(point, avoid, avoid_min_user)
                if point[2] >= avoid_min_user:
                    return True
        return False

    counter = 0
    oldMaximum = ROOT.gPad.GetUymax()
    while isOverlapped():
        #print()

        newMaximum = ROOT.gPad.GetUymax()
        if logy:
            newMaximum = 10**(newMaximum + 0.3) # Since SetMaximum requieres an absolute scale...
        else:
            newMaximum *= 1.05
        parentObject.SetMaximum(newMaximum)
        ROOT.gPad.Update()

        counter += 1
        if counter > 50:
            #Failover for crappy ROOT behaviour
            newMaximum = oldMaximum
            if logy:
                newMaximum = 10**(newMaximum + 3) # Since SetMaximum requieres an absolute scale...
            else:
                newMaximum *= 1.3
            parentObject.SetMaximum(newMaximum)
            ROOT.gPad.Update()
            
            break