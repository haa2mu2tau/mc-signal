#!/bin/env python
import ctypes, re, math
from collections import defaultdict
from pathlib import Path
from typing import Optional, List, Tuple, Union, Dict, Any

import ROOT

from lib.ATLASstyle import ATLASlabel
from lib.plot_utils import autoscalePlot, root_plot_setup

_colors = [ROOT.kGreen - 8, ROOT.kRed - 8, ROOT.kBlue - 8, ROOT.kOrange - 8, ROOT.kYellow - 8, ROOT.kCyan - 8, \
            ROOT.kViolet - 8, ROOT.kBlue, ROOT.kRed, ROOT.kOrange, ROOT.kGreen, ROOT.kYellow, ROOT.kCyan]
_markers = [21, 20, 22, 23, 29, 33, 34, 39, 41, 43, 45, 47]


def plotHist(
    hists: List[ROOT.TH1], 
    histsNames: List[str],
    sampleName: Optional[str]=None, 
    optionalLabel: Optional[str] = None, 
    chainLabel: Optional[str] = None,
    groupLabel: Optional[str] = None,
    energy: str = '13.6 TeV',
    rebin: Optional[Union[List[float], int]]=None, 
    checkRebinConsistency=False, 
    logy: bool = False,
    xLimits: Optional[Tuple[float, float]] = None,
    yLimits: Optional[Tuple[float, float]] = None,
    xLabel: Optional[str] = None,
    yLabel: Optional[str] = None,
    xAlphaBins: Optional[List[str]] = None,
    atlasLabel: str = 'Internal',
    legendColumns: int = 1,
    outputDir: Optional[str] = None, 
    outputPrefix: Optional[str] = None, 
    outputSuffix: Optional[str] = None,
    outputFormat: str = 'png',
    saveDir: Optional[ROOT.TDirectory] = None,
    saveName: Optional[str] = None,
) -> ROOT.TCanvas:
    c = ROOT.TCanvas()
    c.SetFillStyle(0)
    ROOT.gPad.SetTopMargin(0.07)

    leg_topY = 0.89
    legend = ROOT.TLegend(0.57, leg_topY - 0.05 * math.ceil(len(hists)/legendColumns), 0.92, leg_topY)
    ROOT.SetOwnership(legend, False)
    legend.SetTextSize(14)
    legend.SetNColumns(legendColumns)

    hist_stack = ROOT.THStack('h_stack', '')
    plot_hists = []
    for hist, histName, color in zip(hists, histsNames, _colors):
        if rebin:
            if isinstance(rebin, list):
                rebin_C = (ctypes.c_double * len(rebin))(*rebin)

                if checkRebinConsistency:
                    used = [False for _ in range(hist.GetXaxis().GetNbins())]
                    for i, (l, u) in enumerate(zip(rebin, rebin[1:])):
                        for j, j_used in zip(range(1, hist.GetXaxis().GetNbins()+1), used):
                            j_l = hist.GetXaxis().GetBinLowEdge(j)
                            j_u = hist.GetXaxis().GetBinUpEdge(j)
                            if j_l >= l and j_u <= u:
                                if j_used:
                                    raise Exception('Rebinning is incompatible with original binning!')
                                used[j-1] = True

                hist = hist.Rebin(len(rebin)-1, f'{hist.GetName()}_rebinned', rebin_C)
            elif type(rebin) == int:
                hist = hist.Rebin(rebin, f'{hist.GetName()}_rebinned')

        hist.SetLineColor(color)
        hist.SetLineWidth(2)
        hist.SetMarkerStyle(0)

        ROOT.SetOwnership(hist, False)
        plot_hists.append(hist)

        hist_stack.Add(hist, 'hist')
        legend.AddEntry(hist, histName, 'LE')

    hist_stack.Draw('nostack')
    legend.Draw()

    ROOT.gPad.Update()
    if logy:
        c.SetLogy()
    else:
        hist_stack.GetYaxis().SetMaxDigits(3)
        hist_stack.SetMinimum(0)

    hist_stack.GetXaxis().SetTitle(hist.GetXaxis().GetTitle() if xLabel is None else xLabel)
    hist_stack.GetXaxis().SetTitleOffset(1.3)
    hist_stack.GetXaxis().SetTitleSize(23)
    hist_stack.GetXaxis().SetLabelSize(25)
    hist_stack.GetYaxis().SetTitle(hist.GetYaxis().GetTitle() if yLabel is None else yLabel)
    hist_stack.GetYaxis().SetTitleOffset(1.7)
    hist_stack.GetYaxis().SetTitleSize(25)
    hist_stack.GetYaxis().SetLabelSize(25)
    ROOT.gPad.ResizePad()

    if xAlphaBins:
        for i, label in enumerate(xAlphaBins):
            hist_stack.GetXaxis().SetBinLabel(i+1, label)

    lbls = []
    lbls.append(ATLASlabel(0.2, 0.85, atlasLabel, fontSize=23))
    latex = ROOT.TLatex()
    latex.SetTextSize(17)
    latex.SetTextAlign(11)
    lbls.append(latex.DrawLatexNDC(0.2, 0.80, f'#sqrt{{s}} = {energy}' + (f', {sampleName}' if sampleName is not None else '')))
    if optionalLabel:
        lbls.append(latex.DrawLatexNDC(0.2, 0.75, optionalLabel))
    
    if chainLabel:
        latex.SetTextSize(14)
        latex.DrawLatexNDC(0.02, 0.06, chainLabel)
    if groupLabel:
        latex.SetTextSize(14)
        latex.DrawLatexNDC(0.02, 0.02, groupLabel)

    xmin, xmax = hist_stack.GetStack().Last().GetXaxis().GetXmin(), hist_stack.GetStack().Last().GetXaxis().GetXmax()
    if xLimits is not None:
        if xLimits[0] is not None: xmin = xLimits[0]
        if xLimits[1] is not None: xmax = xLimits[1]
        hist_stack.GetXaxis().SetRangeUser(xmin, xmax)

    c.Update()
    autoscalePlot(plot_hists[0], plot_hists, avoidLabels=lbls, avoidLegend=legend, expandAvoidToLeft=True, expandAvoidToRight=True, separationMargin=0.05, )

    if yLimits is not None:
        if yLimits[0] is not None: hist_stack.SetMinimum(yLimits[0])
        if yLimits[1] is not None: hist_stack.SetMaximum(yLimits[1])

    if outputDir is not None:
        outputPrefix = '' if outputPrefix is None else outputPrefix
        outputSuffix = outputSuffix if outputSuffix is not None else hist.GetName()
        c.Print(f'{outputDir}/{outputPrefix}{outputSuffix}.{outputFormat}')

    if saveDir is not None:
        name = saveName if saveName is not None else plot_hists[0].GetName()
        saveDir.WriteTObject(c, name)

    ret_c = c.Clone()
    ROOT.SetOwnership(ret_c, False)
    return ret_c



def plotHist2D(
    hist: ROOT.TH2, 
    sampleName: Optional[str]=None, 
    optionalLabel: Optional[str] = None, 
    chainLabel: Optional[str] = None,
    groupLabel: Optional[str] = None,
    energy: str = '13.6 TeV',
    xLimits: Optional[Tuple[float, float]] = None,
    yLimits: Optional[Tuple[float, float]] = None,
    zLimits: Optional[Tuple[float, float]] = None,
    xLabel: Optional[str] = None,
    yLabel: Optional[str] = None,
    xAlphaBins: Optional[List[str]] = None,
    yAlphaBins: Optional[List[str]] = None,
    atlasLabel: str = 'Internal',
    outputDir: Optional[str] = None, 
    outputPrefix: Optional[str] = None, 
    outputSuffix: Optional[str] = None,
    outputFormat: str = 'eps',
    saveDir: Optional[ROOT.TDirectory] = None,
    saveName: Optional[str] = None,
) -> ROOT.TCanvas:

    canvas_hist = ROOT.TCanvas('canvas', '', 0, 0, 800, 600)
    canvas_hist.SetFillStyle(0)
    hist.Draw("colz")

    if xLimits:
        hist.GetXaxis().SetRangeUser(xLimits[0], xLimits[1])
    if yLimits:
        hist.GetYaxis().SetRangeUser(yLimits[0], yLimits[1])
    if zLimits:
        hist.GetZaxis().SetRangeUser(zLimits[0], zLimits[1])

    hist.GetXaxis().SetTitle(hist.GetXaxis().GetTitle() if xLabel is None else xLabel)
    hist.GetXaxis().SetTitleOffset(1.3)
    hist.GetXaxis().SetTitleSize(23)
    hist.GetXaxis().SetLabelSize(25)
    hist.GetYaxis().SetTitle(hist.GetYaxis().GetTitle() if yLabel is None else yLabel)
    hist.GetYaxis().SetMaxDigits(3)
    hist.GetYaxis().SetTitleOffset(1.7)
    hist.GetYaxis().SetTitleSize(25)
    hist.GetYaxis().SetLabelSize(25)
    hist.GetZaxis().SetTitleOffset(1.1)
    hist.GetZaxis().SetMaxDigits(3)

    if xAlphaBins:
        for i, label in enumerate(xAlphaBins):
            hist.GetXaxis().SetBinLabel(i+1, label)
    if yAlphaBins:
        for i, label in enumerate(yAlphaBins):
            hist.GetYaxis().SetBinLabel(i+1, label)

    ROOT.gPad.SetLeftMargin(0.135)
    ROOT.gPad.SetRightMargin(0.16)
    ROOT.gPad.SetTopMargin(0.07)
    ROOT.gPad.SetBottomMargin(0.18)
    ROOT.gPad.ResizePad()

    ATLASlabel(0.05, 0.03, atlasLabel)

    latex = ROOT.TLatex()
    latex.SetTextSize(22)
    latex.SetTextAlign(12)
    latex.DrawLatexNDC(0.14, 0.96, f'#sqrt{{s}} = {energy}' + (f', {sampleName}' if sampleName is not None else ''))
    
    if optionalLabel:
        latex.SetTextAlign(32)
        latex.DrawLatexNDC(0.84, 0.96, optionalLabel)

    if chainLabel:
        latex.SetTextAlign(12)
        latex.SetTextSize(14)
        latex.DrawLatexNDC(0.05, 0.12, chainLabel)
    if groupLabel:
        latex.SetTextAlign(12)
        latex.SetTextSize(14)
        latex.DrawLatexNDC(0.05, 0.09, groupLabel)
    
    if outputDir is not None:
        outputPrefix = '' if outputPrefix is None else outputPrefix
        outputSuffix = outputSuffix if outputSuffix is not None else hist.GetName()
        canvas_hist.Print(f'{outputDir}/{outputPrefix}{outputSuffix}.{outputFormat}')

    if saveDir is not None:
        name = saveName if saveName is not None else hist.GetName()
        saveDir.WriteTObject(canvas_hist, name)

    ret_c = canvas_hist.Clone()
    ROOT.SetOwnership(ret_c, False)
    return ret_c



def plot_directories(directories: List[ROOT.TDirectory], output_directory: str, labels: Dict[str, str] = {}, format: Dict[str, Dict[str, Any]] = {}, format_override: Dict[str, Any] = {}):
    Path(output_directory).mkdir(parents=True, exist_ok=True)

    root_plot_setup()

    hist_1ds = defaultdict(dict)
    hist_2ds = defaultdict(dict)
    for d in directories:
        for h_name in [key.GetName() for key in d.GetListOfKeys() if key.GetClassName().startswith('TH1')]:
            hist_1ds[h_name][d.GetName()] = (labels[d.GetName()] if labels and d.GetName() in labels else d.GetName(), d.Get(h_name))
        for h_name in [key.GetName() for key in d.GetListOfKeys() if key.GetClassName().startswith('TH2')]:
            hist_2ds[h_name][d.GetName()] = (labels[d.GetName()] if labels and d.GetName() in labels else d.GetName(), d.Get(h_name))

    for h_name, h_sources in hist_1ds.items():
        format_args = {}
        for f_h_name, f_dict in format.items():
            if re.fullmatch(f_h_name, h_name): format_args |= f_dict
        if format_override: format_args |= format_override

        c = plotHist(
            [h for _, h in h_sources.values()],
            [label for label, _ in h_sources.values()],
            **format_args
        )
        c.Print(f'{output_directory}/{h_name}.png')

    for h_name, h_sources in hist_2ds.items():
        format_args = {}
        for f_h_name, f_dict in format.items():
            if re.fullmatch(f_h_name, h_name): format_args |= f_dict
        if format_override: format_args |= format_override

        for directory, (label, hist) in h_sources.items():
            this_sample_name = (f'{format["sampleName"]}, ' if 'sampleName' in format else '') + label
            this_format_args = {k:v for k, v in format_args.items() if k != 'sampleName'}
            c = plotHist2D(
                hist,
                sampleName=this_sample_name,
                **this_format_args
            )
            c.Print(f'{output_directory}/{h_name}_{directory}.png')