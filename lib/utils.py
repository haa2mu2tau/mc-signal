from typing import List, Dict, Optional
import os, glob
from dataclasses import dataclass, field

ana_home = os.getenv('ANA_HOME')
JOs_dir = os.path.join(os.getenv('ANA_HOME'), 'JOs')

def get_JOs(min:Optional[int]=None, max:Optional[int]=None) -> List[int]:
    return [d for d in (int(os.path.basename(p)) for p in glob.glob(os.path.join(JOs_dir, '*'))) if (min is None or min <= d) and (max is None or d <= max)]

@dataclass
class DSID:
    dsid: int
    name: str
    input_LHE: Dict[int, str] = field(default_factory=dict)
    input_EVNT_files: Dict[int, str] = field(default_factory=dict)
    input_DAOD_files: Dict[int, str] = field(default_factory=dict)
    n_files: Optional[int] = None
    analysis: Optional[str] = None